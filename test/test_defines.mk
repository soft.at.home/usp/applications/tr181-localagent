MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../include)

HEADERS = $(wildcard $(INCDIR)/$(COMPONENT)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
          $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. -I../dummy_back_end -I../common \
          -fkeep-inline-functions -fkeep-static-functions \
          -Wno-format-nonliteral -Wno-unused-variable -Wno-unused-but-set-variable \
          -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500 \
          $(shell pkg-config --cflags cmocka)

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
           $(shell pkg-config --libs cmocka) -lamxc -lamxp -lamxj -lamxd -lamxo -lamxb -lamxa \
           -lsahtrace \
           -lamxut \
