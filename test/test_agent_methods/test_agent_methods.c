/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>

#include "dm_controller.h"
#include "dm_agent.h"

#include "test_common.h"
#include "test_agent_methods.h"
#include "dummy_be.h"

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxo_parser_t parser;
static const char* uspagent_definition = "../../odl/tr181-localagent_definition.odl";
static const char* uspagent_defaults = "../common/uspagent_defaults.odl";
static const char* deviceinfo_odl = "../common/deviceinfo.odl";
static int num_on_board_reqs = 0;

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static void connection_read(UNUSED int fd, UNUSED void* priv) {
    // Do nothing
}

static void onboard_event(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    amxc_var_dump(data, STDOUT_FILENO);

    num_on_board_reqs++;
    assert_true(test_verify_data(data, "agent_supported_protocol_version", AGENT_SUPPORTED_PROTOCOL_VERSIONS));
    assert_true(test_verify_data(data, "oui", "AABBCC"));
    assert_true(test_verify_data(data, "product_class", "IB2"));
    assert_true(test_verify_data(data, "serial_number", "123456789"));
    assert_non_null(GETP_ARG(data, "agent_eid"));
    assert_non_null(GETP_ARG(data, "contr_eid"));
}

static void transfer_complete_cb(UNUSED const char* const sig_name,
                                 const amxc_var_t* const data,
                                 UNUSED void* const priv) {
    printf("Callback function for TransferComplete! event\n");
    fflush(stdout);
    amxc_var_dump(data, STDOUT_FILENO);

    test_verify_data(data, "data.CommandKey", "dummy");
}

int test_uspa_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    assert_int_equal(test_register_dummy_be(), 0);
    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "controller_send_on_board_request", AMXO_FUNC(_controller_send_on_board_request));
    assert_int_equal(amxo_parser_parse_file(&parser, deviceinfo_odl, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, uspagent_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, uspagent_defaults, root_obj), 0);

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    amxb_register(bus_ctx, &dm);

    amxp_sigmngr_add_signal(&dm.sigmngr, "OnBoardRequest!");

    assert_int_equal(_la_main(0, &dm, &parser), 0);
    assert_int_equal(_la_main(2, &dm, &parser), 0);
    handle_events();

    return 0;
}

int test_uspa_teardown(UNUSED void** state) {
    amxp_signal_t* sig = amxp_sigmngr_find_signal(&dm.sigmngr, "OnBoardRequest!");

    amxp_sigmngr_remove_signal(&dm.sigmngr, "OnBoardRequest!");
    amxp_signal_delete(&sig);

    assert_int_equal(_la_main(1, &dm, &parser), 0);

    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_can_send_onboard_request(UNUSED void** state) {
    amxd_object_t* contr = amxd_dm_findf(&dm, "LocalAgent.Controller.1.");
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    assert_non_null(contr);

    // Set OnBoardingComplete to true and check it is set to false by SendOnBoardRequest()
    amxd_trans_select_object(&trans, contr);
    amxd_trans_set_value(bool, &trans, "OnBoardingComplete", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);

    handle_events();
    assert_true(amxd_object_get_value(bool, contr, "OnBoardingComplete", NULL));

    assert_int_equal(amxp_slot_connect(&dm.sigmngr, "OnBoardRequest!", NULL, onboard_event, NULL), 0);
    _Controller_SendOnBoardRequest(contr, NULL, NULL, NULL);

    handle_events();
    assert_int_equal(num_on_board_reqs, 1);

    assert_false(amxd_object_get_value(bool, contr, "OnBoardingComplete", NULL));

    amxd_trans_clean(&trans);
}

void test_can_send_transfer_complete(UNUSED void** state) {
    amxd_object_t* object = amxd_dm_findf(&dm, "LocalAgent.");
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "CommandKey", "dummy");

    amxb_subscribe(bus_ctx, "LocalAgent.", "notification == 'TransferComplete!'", transfer_complete_cb, NULL);

    _SendTransferComplete(object, NULL, &args, NULL);

    handle_events();

    amxc_var_clean(&args);
}

void test_can_get_mtp_info(UNUSED void** state) {
    amxd_object_t* contr = amxd_dm_findf(&dm, "LocalAgent.Controller.1.");
    amxc_var_t ret;

    amxc_var_init(&ret);

    assert_non_null(contr);

    _Controller_GetMTPInfo(contr, NULL, NULL, &ret);

    amxc_var_dump(&ret, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&ret, "protocol"), "MQTT");
    assert_string_equal(GET_CHAR(&ret, "topic"), "test-topic");

    amxc_var_clean(&ret);
}

void test_can_send_onboard_request_from_param(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LocalAgent.Controller.1.");
    amxd_trans_set_value(bool, &trans, "SendOnBoardRequest", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);

    handle_events();
    assert_int_equal(num_on_board_reqs, 2);

    amxd_trans_clean(&trans);
}
