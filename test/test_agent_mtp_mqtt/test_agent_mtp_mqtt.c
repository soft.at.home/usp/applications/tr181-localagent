/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>

#include "dm_mtp_mqtt_events.h"

#include "test_common.h"
#include "test_agent_mtp_mqtt.h"
#include "dummy_be.h"

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxo_parser_t parser;
static const char* uspagent_definition = "../../odl/tr181-localagent_definition.odl";
static const char* uspagent_defaults = "./uspagent_defaults.odl";
static const char* mqtt_definition = "../common/tr181-mqtt_definition.odl";
static const char* mqtt_defaults = "../common/tr181-mqtt_defaults.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static void connection_read(UNUSED int fd, UNUSED void* priv) {
    // Do nothing
}

int test_uspa_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    assert_int_equal(test_register_dummy_be(), 0);
    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "mtp_mqtt_update_subs", AMXO_FUNC(_mtp_mqtt_update_subs));
    assert_int_equal(amxo_parser_parse_file(&parser, uspagent_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, uspagent_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mqtt_defaults, root_obj), 0);

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    amxb_register(bus_ctx, &dm);

    assert_int_equal(_la_main(0, &dm, &parser), 0);
    assert_int_equal(_la_main(2, &dm, &parser), 0);
    handle_events();

    return 0;
}

int test_uspa_teardown(UNUSED void** state) {
    assert_int_equal(_la_main(1, &dm, &parser), 0);

    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_setting_resp_topic_creates_sub(UNUSED void** state) {
    amxd_object_t* mqtt = amxd_dm_findf(&dm, "MQTT.Client.1.");
    amxd_object_t* sub = amxd_dm_findf(&dm, "MQTT.Client.1.Subscription.1.");
    char* topic = NULL;
    amxd_trans_t trans;

    assert_non_null(mqtt);
    assert_null(sub);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LocalAgent.MTP.1.MQTT.");
    amxd_trans_set_value(cstring_t, &trans, "ResponseTopicConfigured", "initial");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);

    handle_events();

    sub = amxd_dm_findf(&dm, "MQTT.Client.1.Subscription.1.");
    assert_non_null(sub);

    topic = amxd_object_get_value(cstring_t, sub, "Topic", NULL);
    assert_string_equal(topic, "initial");

    free(topic);
    amxd_trans_clean(&trans);
}

void test_changing_resp_topic_recreates_sub(UNUSED void** state) {
    amxd_object_t* mqtt = amxd_dm_findf(&dm, "MQTT.Client.1.");
    amxd_object_t* sub = amxd_dm_findf(&dm, "MQTT.Client.1.Subscription.1.");
    char* topic = NULL;
    amxd_trans_t trans;

    assert_non_null(mqtt);
    assert_non_null(sub);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LocalAgent.MTP.1.MQTT.");
    amxd_trans_set_value(cstring_t, &trans, "ResponseTopicConfigured", "updated");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);

    handle_events();

    sub = amxd_dm_findf(&dm, "MQTT.Client.1.Subscription.1.");
    assert_null(sub);

    sub = amxd_dm_findf(&dm, "MQTT.Client.1.Subscription.2.");
    assert_non_null(sub);

    topic = amxd_object_get_value(cstring_t, sub, "Topic", NULL);
    assert_string_equal(topic, "updated");

    free(topic);
    amxd_trans_clean(&trans);
}
