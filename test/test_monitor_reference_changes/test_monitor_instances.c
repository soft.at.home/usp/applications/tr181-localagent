/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <amxo/amxo.h>

#include <amxc/amxc_macros.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_timer.h>

#include "localagent.h"
#include "dm_monitor.h"
#include "test_monitor_instances.h"


#define GET_KEY(v, k) amxc_var_get_key(v, k, AMXC_VAR_FLAG_DEFAULT)
#define CHAR(v) amxc_var_constcast(cstring_t, v)
#define UINT32(v) amxc_var_constcast(uint32_t, v)

static const char* odl_defs = "../../odl/tr181-localagent_definition.odl";
static const char* test_odl = "./phonebook.odl";

static amxd_dm_t* dm;
static amxb_bus_ctx_t* bus_ctx;


typedef struct _event_check {
    uint32_t counter;
    amxc_var_t event_data;
} event_check_t;


int test_monitor_reference_setup(void** state) {
    SAH_TRACEZ_ERROR("Test", "Add instances setup");
    amxut_bus_setup(state);
    bus_ctx = amxut_bus_ctx();
    dm = amxut_bus_dm();
    amxut_resolve_function("check_monitor", _check_monitor);
    amxut_resolve_function("Monitor_Added", _Monitor_Added);
    amxut_resolve_function("Monitor_Changed", _Monitor_Changed);
    amxut_dm_load_odl(odl_defs);
    amxut_dm_load_odl(test_odl);

    assert_int_equal(_la_main(0, amxut_bus_dm(), amxut_bus_parser()), 0);
    assert_int_equal(_la_main(2, amxut_bus_dm(), amxut_bus_parser()), 0);
    return 0;
}

int test_monitor_reference_teardown(void** state) {

    SAH_TRACEZ_ERROR("Test", "teardown");

    assert_int_equal(_la_main(1, amxut_bus_dm(), amxut_bus_parser()), 0);
    amxut_bus_teardown(state);
    return 0;
}

static void test_add_monitor_instance(bool enable, uint32_t interval, const char* reference_list, const char* controller, amxc_var_t* ret) {
    amxc_var_t* params = NULL;
    amxc_var_t args;
    amxd_object_t* monitor = amxd_dm_findf(amxut_bus_dm(), "LocalAgent.Monitor.");
    assert_non_null(monitor);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, params, "Enable", enable);
    amxc_var_add_key(uint32_t, params, "Interval", interval);
    amxc_var_add_key(cstring_t, params, "ReferenceList", reference_list);
    amxc_var_add_key(cstring_t, params, "Controller", controller);

    assert_int_equal(amxd_object_invoke_function(monitor, "_add", &args, ret), amxd_status_ok);
    assert_false(amxc_var_is_null(ret));
    amxc_var_clean(&args);
}

static void test_onchange_triggered_check_new_value(const char* const sig_name,
                                                    const amxc_var_t* const data,
                                                    void* const priv) {
    event_check_t* check = (event_check_t*) (priv);
    amxc_var_t* changeset = GET_ARG(data, "data");
    assert_string_equal(sig_name, "OnChange!");

    printf("==================================\n%s\n", sig_name);
    fflush(stdout);
    amxc_var_dump(data, STDOUT_FILENO);
    printf("==================================\n");
    fflush(stdout);


    assert_string_equal(GET_CHAR(changeset, "ChangeSet.1.Parameter.1.Name"), GET_CHAR(&check->event_data, "Name"));
    assert_string_equal(GET_CHAR(changeset, "ChangeSet.1.Parameter.1.Value"), GET_CHAR(&check->event_data, "Value"));
    assert_string_equal(GET_CHAR(changeset, "ChangeSet.1.Parameter.1.OldValue"), GET_CHAR(&check->event_data, "OldValue"));
    check->counter++;
}

void test_monitor_single_object(UNUSED void** state) {
    SAH_TRACEZ_ERROR("Test", "TEST: Monitor single object change");
    amxc_var_t ret;
    amxc_var_t args;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

// add initial contact
    amxc_var_add_key(cstring_t, &args, "FirstName", "John");
    amxc_var_add_key(cstring_t, &args, "LastName", "Doe");
    assert_int_equal(amxb_add(amxut_bus_ctx(), "Phonebook.Contact.", 0, NULL, &args, &ret, 5), 0);

// build check for later
    event_check_t check;
    check.counter = 0;
    amxc_var_init(&check.event_data);
    amxc_var_set_type(&check.event_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &check.event_data, "Name", "FirstName");
    amxc_var_add_key(cstring_t, &check.event_data, "Value", "Jane");
    amxc_var_add_key(cstring_t, &check.event_data, "OldValue", "John");

// add monitor instance
    test_add_monitor_instance(true, 10000000, "Phonebook.", "Controller", &ret);
    assert_string_equal(CHAR(GET_KEY(&ret, "name")), "cpe-Monitor-1");
    assert_int_equal(UINT32(GET_KEY(&ret, "index")), 1);
    amxp_slot_connect(&dm->sigmngr, "OnChange!", NULL, test_onchange_triggered_check_new_value, &check);

// change 1 param
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FirstName", "Jane");
    amxb_set(bus_ctx, "Phonebook.Contact.1.", &args, &ret, 5);

    amxut_timer_go_to_future_ms(10000);

    assert_int_equal(check.counter, 1);

    amxp_slot_disconnect(&dm->sigmngr, "OnChange!", test_onchange_triggered_check_new_value);
    amxc_var_clean(&ret);
    amxc_var_clean(&check.event_data);
}

void test_monitor_single_param(UNUSED void** state) {
    SAH_TRACEZ_ERROR("Test", "TEST: Monitor single parameter change");
    amxc_var_t ret;
    amxc_var_t args;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

// add initial contact
    amxc_var_add_key(cstring_t, &args, "FirstName", "John");
    amxc_var_add_key(cstring_t, &args, "LastName", "Doe");
    assert_int_equal(amxb_add(amxut_bus_ctx(), "Phonebook.Contact.", 0, NULL, &args, &ret, 5), 0);

// build check for later
    event_check_t check;
    check.counter = 0;
    amxc_var_init(&check.event_data);
    amxc_var_set_type(&check.event_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &check.event_data, "Name", "FirstName");
    amxc_var_add_key(cstring_t, &check.event_data, "Value", "Jane");
    amxc_var_add_key(cstring_t, &check.event_data, "OldValue", "John");

// add monitor instance
    test_add_monitor_instance(true, 10000000, "Phonebook.Contact.*.FirstName", "Controller", &ret);
    assert_string_equal(CHAR(GET_KEY(&ret, "name")), "cpe-Monitor-1");
    assert_int_equal(UINT32(GET_KEY(&ret, "index")), 1);
    amxp_slot_connect(&dm->sigmngr, "OnChange!", NULL, test_onchange_triggered_check_new_value, &check);

// change 1 param
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FirstName", "Jane");
    amxb_set(bus_ctx, "Phonebook.Contact.1.", &args, &ret, 5);

    amxut_timer_go_to_future_ms(10000);

    assert_int_equal(check.counter, 1);

    amxp_slot_disconnect(&dm->sigmngr, "OnChange!", test_onchange_triggered_check_new_value);
    amxc_var_clean(&ret);
    amxc_var_clean(&check.event_data);
}

void test_monitor_wrong_param(UNUSED void** state) {
    SAH_TRACEZ_ERROR("Test", "TEST: Monitor single parameter change of parameter that is not in the referencelist");
    amxc_var_t ret;
    amxc_var_t args;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

// add initial contact
    amxc_var_add_key(cstring_t, &args, "FirstName", "John");
    amxc_var_add_key(cstring_t, &args, "LastName", "Doe");
    assert_int_equal(amxb_add(amxut_bus_ctx(), "Phonebook.Contact.", 0, NULL, &args, &ret, 5), 0);

// build check for later
    event_check_t check;
    check.counter = 0;
    amxc_var_init(&check.event_data);
    amxc_var_set_type(&check.event_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &check.event_data, "Name", "FirstName");
    amxc_var_add_key(cstring_t, &check.event_data, "Value", "Jane");
    amxc_var_add_key(cstring_t, &check.event_data, "OldValue", "John");

// add monitor instance
    test_add_monitor_instance(true, 10000000, "Phonebook.Contact.*.LastName", "Controller", &ret);
    assert_string_equal(CHAR(GET_KEY(&ret, "name")), "cpe-Monitor-1");
    assert_int_equal(UINT32(GET_KEY(&ret, "index")), 1);
    amxp_slot_connect(&dm->sigmngr, "OnChange!", NULL, test_onchange_triggered_check_new_value, &check);

// change 1 param
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FirstName", "Jane");
    amxb_set(bus_ctx, "Phonebook.Contact.1.", &args, &ret, 5);

    amxut_timer_go_to_future_ms(10000);

    assert_int_equal(check.counter, 0);

    amxp_slot_disconnect(&dm->sigmngr, "OnChange!", test_onchange_triggered_check_new_value);
    amxc_var_clean(&ret);
    amxc_var_clean(&check.event_data);
}

void test_monitor_disable(UNUSED void** state) {
    SAH_TRACEZ_ERROR("Test", "TEST: Monitor single object change");
    amxc_var_t ret;
    amxc_var_t args;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

// check monitor as in test_monitor_single_object
    amxc_var_add_key(cstring_t, &args, "FirstName", "John");
    amxc_var_add_key(cstring_t, &args, "LastName", "Doe");
    assert_int_equal(amxb_add(amxut_bus_ctx(), "Phonebook.Contact.", 0, NULL, &args, &ret, 5), 0);
    event_check_t check;
    check.counter = 0;
    amxc_var_init(&check.event_data);
    amxc_var_set_type(&check.event_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &check.event_data, "Name", "FirstName");
    amxc_var_add_key(cstring_t, &check.event_data, "Value", "Jane");
    amxc_var_add_key(cstring_t, &check.event_data, "OldValue", "John");
    test_add_monitor_instance(true, 10000000, "Phonebook.", "Controller", &ret);
    assert_string_equal(CHAR(GET_KEY(&ret, "name")), "cpe-Monitor-1");
    assert_int_equal(UINT32(GET_KEY(&ret, "index")), 1);
    amxp_slot_connect(&dm->sigmngr, "OnChange!", NULL, test_onchange_triggered_check_new_value, &check);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FirstName", "Jane");
    amxb_set(bus_ctx, "Phonebook.Contact.1.", &args, &ret, 5);

    amxut_timer_go_to_future_ms(10000);
    assert_int_equal(check.counter, 1);

// No new change sends no new event
    amxut_timer_go_to_future_ms(10000);
    assert_int_equal(check.counter, 1);

// New change sends new event
    amxc_var_set_type(&check.event_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &check.event_data, "Name", "FirstName");
    amxc_var_add_key(cstring_t, &check.event_data, "Value", "Jake");
    amxc_var_add_key(cstring_t, &check.event_data, "OldValue", "Jane");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FirstName", "Jake");
    amxb_set(bus_ctx, "Phonebook.Contact.1.", &args, &ret, 5);
    amxut_timer_go_to_future_ms(10000);
    assert_int_equal(check.counter, 2);

// Disable monitor and do new change
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "Enable", false);
    amxb_set(bus_ctx, "LocalAgent.Monitor.1.", &args, &ret, 5);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "FirstName", "Josh");
    amxb_set(bus_ctx, "Phonebook.Contact.1.", &args, &ret, 5);
    amxut_timer_go_to_future_ms(10000);
    assert_int_equal(check.counter, 2);


    amxp_slot_disconnect(&dm->sigmngr, "OnChange!", test_onchange_triggered_check_new_value);
    amxc_var_clean(&ret);
    amxc_var_clean(&check.event_data);
}