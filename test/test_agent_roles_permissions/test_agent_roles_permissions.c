/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <dirent.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <sys/stat.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include "localagent.h"
#include "dm_controllertrust.h"
#include "test_agent_roles_permissions.h"

#define UNUSED __attribute__((unused))

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxo_parser_t parser;
static const char* odl_definition = "../../odl/tr181-localagent_definition.odl";
static const char* odl_defaults = "tr181-localagent_defaults.odl";
static const char* odl_config = "test.odl";

static void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
    fflush(stdout);
}

static bool controllertrust_dir_exists(const char* dir_name) {
    bool exists = false;
    DIR* dir = opendir(dir_name);
    if(dir) {
        /* Directory exists. */
        printf("dir exists\n");
        exists = true;
        closedir(dir);
    } else {
        printf("dir does not exist\n");
    }
    return exists;
}

int test_uspa_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    mkdir("./acldir", 0700);

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "Role_Added", AMXO_FUNC(_Role_Added));
    amxo_resolver_ftab_add(&parser, "Permission_Added", AMXO_FUNC(_Permission_Added));
    amxo_resolver_ftab_add(&parser, "Permission_Changed", AMXO_FUNC(_Permission_Changed));
    //amxo_resolver_ftab_add(&parser, "role_instance_valid", AMXO_FUNC(_role_instance_valid));
    assert_int_equal(amxo_parser_parse_file(&parser, odl_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defaults, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_config, root_obj), 0);

    handle_events();

    assert_int_equal(_la_main(0, &dm, &parser), 0);
    assert_int_equal(_la_main(2, &dm, &parser), 0);

    return 0;
}

int test_uspa_teardown(UNUSED void** state) {
    system("rm -r ./acldir");
    handle_events();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_create_role(UNUSED void** state) {
    printf("Creating role\n");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* role_obj = amxd_dm_findf(&dm, "LocalAgent.ControllerTrust.Role");

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, params, "Alias", "Test1");
    amxc_var_add_key(cstring_t, params, "Name", "unit-test");
    amxc_var_add_key(bool, params, "Enable", false);
    assert_int_equal(amxd_object_invoke_function(role_obj, "_add", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    handle_events();

    printf("looking for role: %s\n", GET_CHAR(&ret, "name"));
    role_obj = amxd_dm_findf(&dm, "LocalAgent.ControllerTrust.Role.%s", GET_CHAR(&ret, "name"));
    assert_non_null(role_obj);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(role_obj, "_get", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    assert_true(controllertrust_dir_exists("acldir/unit-test"));
}

void test_create_role_no_name(UNUSED void** state) {
    printf("Creating role\n");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* role_obj = amxd_dm_findf(&dm, "LocalAgent.ControllerTrust.Role");

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, params, "Alias", "Test2");
    amxc_var_add_key(bool, params, "Enable", false);
    assert_int_equal(amxd_object_invoke_function(role_obj, "_add", &args, &ret), 0);

    amxb_get(agent_get_busctxt(), "LocalAgent.ControllerTrust.Role", -1, &ret, 5);
    assert_string_equal(GETP_CHAR(&ret, "parameters.Alias"), "Test2");
    assert_string_equal(GETP_CHAR(&ret, "parameters.Name"), "");

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_create_role_name_already_used(UNUSED void** state) {
    printf("Creating role\n");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* role_obj = amxd_dm_findf(&dm, "LocalAgent.ControllerTrust.Role");

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, params, "Alias", "Test3");
    amxc_var_add_key(cstring_t, params, "Name", "unit-test");
    amxc_var_add_key(bool, params, "Enable", false);
    //assert_int_equal(amxd_object_invoke_function(role_obj, "_add", &args, &ret), amxd_status_invalid_name);
    printf("ret is\n");
    fflush(stdout);
    amxc_var_dump(&ret, STDOUT_FILENO);

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    //assert_true(controllertrust_dir_exists("acldir/Test2"));
}

void test_create_disabled_permission(UNUSED void** state) {
    printf("create disabled permission\n");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* permission_obj = amxd_dm_findf(&dm, "LocalAgent.ControllerTrust.Role.1.Permission");

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, params, "Alias", "disabled_permission");
    amxc_var_add_key(cstring_t, params, "CommandEvent", "rwxn");
    amxc_var_add_key(cstring_t, params, "Obj", "rwxn");
    amxc_var_add_key(cstring_t, params, "InstantiatedObj", "rwxn");
    amxc_var_add_key(cstring_t, params, "Param", "rwxn");
    amxc_var_add_key(uint32_t, params, "Order", 20);
    amxc_var_add_key(cstring_t, params, "Targets", "test_target");
    amxc_var_add_key(bool, params, "Enable", false);
    assert_int_equal(amxd_object_invoke_function(permission_obj, "_add", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    handle_events();

    permission_obj = amxd_dm_findf(&dm, "LocalAgent.ControllerTrust.Role.1.Permission.1.");
    assert_non_null(permission_obj);

    assert_false(access("acldir/unit-test/disabled_permission.json", F_OK) == 0);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_create_enabled_permission(UNUSED void** state) {
    printf("create enabled permission\n");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* params = NULL;
    amxd_object_t* permission_obj = amxd_dm_findf(&dm, "LocalAgent.ControllerTrust.Role.Test1.Permission");

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    params = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, params, "Alias", "enabled_permission");
    amxc_var_add_key(cstring_t, params, "CommandEvent", "rwxn");
    amxc_var_add_key(cstring_t, params, "Obj", "rwxn");
    amxc_var_add_key(cstring_t, params, "InstantiatedObj", "rwxn");
    amxc_var_add_key(cstring_t, params, "Param", "rwxn");
    amxc_var_add_key(uint32_t, params, "Order", 20);
    amxc_var_add_key(cstring_t, params, "Targets", "test_target");
    amxc_var_add_key(bool, params, "Enable", true);
    assert_int_equal(amxd_object_invoke_function(permission_obj, "_add", &args, &ret), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);

    handle_events();

    permission_obj = amxd_dm_findf(&dm, "LocalAgent.ControllerTrust.Role.Test1.Permission.enabled_permission");
    assert_non_null(permission_obj);

    assert_true(access("acldir/unit-test/enabled_permission.json", F_OK) == 0);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
