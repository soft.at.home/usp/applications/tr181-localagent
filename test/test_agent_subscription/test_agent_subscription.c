/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxp/amxp.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_timer.h>

#include "dm_subscription.h"

#include "test_agent_subscription.h"
#include "dummy_be.h"

static amxd_dm_t dm;
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxo_parser_t parser;
static const char* uspagent_definition = "../../odl/tr181-localagent_definition.odl";
static const char* uspagent_defaults = "../common/uspagent_defaults.odl";
static const char* phonebook_odl = "./phonebook.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

static void connection_read(UNUSED int fd, UNUSED void* priv) {
    // Do nothing
}

static void remove_sub(const char* sub_alias) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, amxd_dm_findf(&dm, "LocalAgent.Subscription."));
    amxd_trans_del_inst(&trans, 0, sub_alias);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();
}

int test_uspa_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    assert_int_equal(test_register_dummy_be(), 0);
    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    amxo_resolver_ftab_add(&parser, "check_subscription", AMXO_FUNC(_check_subscription));
    amxo_resolver_ftab_add(&parser, "dm_subscription_assign_default", AMXO_FUNC(_dm_subscription_assign_default));
    amxo_resolver_ftab_add(&parser, "Subscription_Added", AMXO_FUNC(_Subscription_Added));
    amxo_resolver_ftab_add(&parser, "Subscription_Changed", AMXO_FUNC(_Subscription_Changed));
    amxo_resolver_ftab_add(&parser, "dm_subscription_cleanup", AMXO_FUNC(_dm_subscription_cleanup));

    assert_int_equal(amxo_parser_parse_file(&parser, phonebook_odl, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, uspagent_definition, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, uspagent_defaults, root_obj), 0);

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxb_set_access(bus_ctx, AMXB_PUBLIC);
    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    amxb_register(bus_ctx, &dm);

    assert_int_equal(_la_main(0, &dm, &parser), 0);
    assert_int_equal(_la_main(2, &dm, &parser), 0);
    handle_events();

    return 0;
}

int test_uspa_teardown(UNUSED void** state) {
    amxb_free(&bus_ctx);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_add_subscription_valchange_object(UNUSED void** state) {
    amxd_object_t* sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    amxd_trans_t trans;
    amxc_var_t ret;
    amxc_var_t* get_response = NULL;

    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 0, "sub_1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "ID", "add_sub");
    amxd_trans_set_value(cstring_t, &trans, "NotifType", "ValueChange");
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.unit-test.");
    amxd_trans_set_value(cstring_t, &trans, "ReferenceList", "Device.Phonebook.Contact.1.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.sub_1.", INT32_MAX, &ret, 5), 0);

    get_response = GETP_ARG(&ret, "0.0");
    assert_non_null(get_response);
    assert_false(amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, get_response)));
    assert_int_equal(strcmp("add_sub", GET_CHAR(get_response, "ID")), 0);

    remove_sub("sub_1");
    amxc_var_clean(&ret);
}

void test_add_subscription_valchange_parameter(UNUSED void** state) {
    amxd_object_t* sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    amxd_trans_t trans;
    amxc_var_t ret;
    amxc_var_t* get_response = NULL;

    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 0, "sub_2");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "ID", "add_sub_param");
    amxd_trans_set_value(cstring_t, &trans, "NotifType", "ValueChange");
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.unit-test.");
    amxd_trans_set_value(cstring_t, &trans, "ReferenceList", "Device.Phonebook.Contact.1.FirstName");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.sub_2.", INT32_MAX, &ret, 5), 0);

    get_response = GETP_ARG(&ret, "0.0");
    assert_non_null(get_response);
    assert_false(amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, get_response)));
    assert_int_equal(strcmp("add_sub_param", GET_CHAR(get_response, "ID")), 0);

    remove_sub("sub_2");
    amxc_var_clean(&ret);
}

void test_add_subscription_valchange_searchpath(UNUSED void** state) {
    amxd_object_t* sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    amxd_trans_t trans;
    amxc_var_t ret;
    amxc_var_t* get_response = NULL;

    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 0, "sub_3");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "ID", "add_sub_search");
    amxd_trans_set_value(cstring_t, &trans, "NotifType", "ValueChange");
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.unit-test.");
    amxd_trans_set_value(cstring_t, &trans, "ReferenceList", "Device.Phonebook.Contact.[FirstName=='John'].");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.sub_3.", INT32_MAX, &ret, 5), 0);

    get_response = GETP_ARG(&ret, "0.0");
    assert_non_null(get_response);
    assert_false(amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, get_response)));
    assert_int_equal(strcmp("add_sub_search", GET_CHAR(get_response, "ID")), 0);

    amxc_var_clean(&ret);
}

void test_add_subscription_valchange_used_ID(UNUSED void** state) {
    amxd_object_t* sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    amxd_trans_t trans;
    amxc_var_t ret;

    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 0, "sub_4");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "ID", "add_sub_search");
    amxd_trans_set_value(cstring_t, &trans, "NotifType", "ValueChange");
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.unit-test.");
    amxd_trans_set_value(cstring_t, &trans, "ReferenceList", "Device.Phonebook.Contact.1.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_duplicate);
    amxd_trans_clean(&trans);

    handle_events();

    assert_int_not_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.sub_4.", INT32_MAX, &ret, 5), 0);

    remove_sub("sub_3");
    amxc_var_clean(&ret);
}

void test_add_subscription_object_creation(UNUSED void** state) {
    amxd_object_t* sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    amxd_trans_t trans;
    amxc_var_t ret;
    amxc_var_t* get_response = NULL;

    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 0, "sub_5");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "ID", "add_sub");
    amxd_trans_set_value(cstring_t, &trans, "NotifType", "ObjectCreation");
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.unit-test.");
    amxd_trans_set_value(cstring_t, &trans, "ReferenceList", "Device.Phonebook.Contact.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.sub_5.", INT32_MAX, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    get_response = GETP_ARG(&ret, "0.0");
    assert_non_null(get_response);
    assert_false(amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, get_response)));
    assert_int_equal(strcmp("add_sub", GET_CHAR(get_response, "ID")), 0);

    remove_sub("sub_5");
    amxc_var_clean(&ret);
}

void test_add_subscription_object_creation_searchpath(UNUSED void** state) {
    amxd_object_t* sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    amxd_trans_t trans;
    amxc_var_t ret;
    amxc_var_t* get_response = NULL;

    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 0, "sub_6");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "ID", "add_search_sub");
    amxd_trans_set_value(cstring_t, &trans, "NotifType", "ObjectCreation");
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.unit-test.");
    amxd_trans_set_value(cstring_t, &trans, "ReferenceList", "Device.Phonebook.Contact.[FirstName=='John'].PhoneNumber.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.sub_6.", INT32_MAX, &ret, 5), 0);
    get_response = GETP_ARG(&ret, "0.0");
    assert_non_null(get_response);
    assert_false(amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, get_response)));
    assert_int_equal(strcmp("add_search_sub", GET_CHAR(get_response, "ID")), 0);

    remove_sub("sub_6");
    amxc_var_clean(&ret);
}

void test_add_subscription_object_creation_instance(UNUSED void** state) {
    amxd_object_t* sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    amxd_trans_t trans;
    amxc_var_t ret;

    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 0, "sub_7");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "ID", "add_instance_sub");
    amxd_trans_set_value(cstring_t, &trans, "NotifType", "ObjectCreation");
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.unit-test.");
    amxd_trans_set_value(cstring_t, &trans, "ReferenceList", "Device.Phonebook.Contact.1.");
    assert_int_not_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    assert_int_not_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.sub_7.", INT32_MAX, &ret, 5), 0);

    amxc_var_clean(&ret);
}

void test_add_subscription_object_creation_parameter(UNUSED void** state) {
    amxd_object_t* sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    amxd_trans_t trans;
    amxc_var_t ret;

    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 0, "sub_8");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "ID", "add_param_sub");
    amxd_trans_set_value(cstring_t, &trans, "NotifType", "ObjectCreation");
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.unit-test.");
    amxd_trans_set_value(cstring_t, &trans, "ReferenceList", "Device.Phonebook.Contact.1.FirstName");
    assert_int_not_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    assert_int_not_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.sub_8.", INT32_MAX, &ret, 5), 0);

    amxc_var_clean(&ret);
}

void test_add_subscription_object_deletion(UNUSED void** state) {
    amxd_object_t* sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    amxd_trans_t trans;
    amxc_var_t ret;
    amxc_var_t* get_response = NULL;

    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 0, "sub_9");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "ID", "del_sub");
    amxd_trans_set_value(cstring_t, &trans, "NotifType", "ObjectDeletion");
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.unit-test.");
    amxd_trans_set_value(cstring_t, &trans, "ReferenceList", "Device.Phonebook.Contact.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.sub_9.", INT32_MAX, &ret, 5), 0);
    get_response = GETP_ARG(&ret, "0.0");
    assert_non_null(get_response);
    assert_false(amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, get_response)));
    assert_int_equal(strcmp("del_sub", GET_CHAR(get_response, "ID")), 0);

    remove_sub("sub_9");
    amxc_var_clean(&ret);
}

void test_can_add_persistent_sub(UNUSED void** state) {
    amxd_object_t* object = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    amxd_trans_t trans;
    uint32_t attrs = 0;

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_add_inst(&trans, 0, "persist-sub");
    amxd_trans_set_value(cstring_t, &trans, "ID", "persist_sub");
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.unit-test.");
    amxd_trans_set_value(bool, &trans, "Persistent", true);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxc_var_dump(&trans.retvals, STDOUT_FILENO);

    handle_events();

    object = amxd_dm_findf(&dm, "%s", GETP_CHAR(&trans.retvals, "1.path"));
    assert_non_null(object);

    attrs = amxd_object_get_attrs(object);
    assert_true(IS_BIT_SET(attrs, amxd_oattr_persistent));

    amxd_trans_clean(&trans);
}

void test_can_change_persistency_of_sub(UNUSED void** state) {
    amxd_object_t* object = amxd_dm_findf(&dm, "LocalAgent.Subscription.persist-sub.");
    amxd_trans_t trans;
    uint32_t attrs = 0;

    assert_non_null(object);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Persistent", false);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxc_var_dump(&trans.retvals, STDOUT_FILENO);

    handle_events();

    attrs = amxd_object_get_attrs(object);
    assert_false(IS_BIT_SET(attrs, amxd_oattr_persistent));

    remove_sub("persist-sub");
    amxd_trans_clean(&trans);
}

void test_last_value_is_saved_on_exit(UNUSED void** state) {
    amxd_object_t* object = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    char* last_value = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_add_inst(&trans, 0, "param-sub");
    amxd_trans_set_value(csv_string_t, &trans, "ReferenceList", "Device.Phonebook.Contact.1.FirstName");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "NotifType", "ValueChange");
    amxd_trans_set_value(cstring_t, &trans, "ID", "param-sub");
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.unit-test.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxc_var_dump(&trans.retvals, STDOUT_FILENO);

    handle_events();

    assert_int_equal(_la_main(1, &dm, &parser), 0);

    object = amxd_dm_findf(&dm, "LocalAgent.Subscription.param-sub.");
    assert_non_null(object);
    last_value = amxd_object_get_value(cstring_t, object, "LastValue", NULL);
    assert_string_equal(last_value, "John");

    free(last_value);
    amxd_trans_clean(&trans);
}

void test_can_add_sub_without_id(UNUSED void** state) {
    amxd_object_t* sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    amxd_trans_t trans;
    amxc_var_t ret;
    amxc_var_t* id = NULL;
    const char* alias = "sub-no-id";

    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 0, alias);
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.unit-test.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.sub-no-id.", INT32_MAX, &ret, 5), 0);
    id = GETP_ARG(&ret, "0.0.ID");
    assert_non_null(id);

    remove_sub(alias);
    amxc_var_clean(&ret);
}

void test_can_add_multiple_subs_without_id(UNUSED void** state) {
    amxd_object_t* sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    amxd_trans_t trans;
    amxc_var_t ret;
    amxc_var_t* id = NULL;
    const char* alias_1 = "sub-no-id-1";
    const char* alias_2 = "sub-no-id-2";

    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 0, alias_1);
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.unit-test.");
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 0, alias_2);
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.unit-test.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.sub-no-id-1.", INT32_MAX, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    id = GETP_ARG(&ret, "0.0.ID");
    assert_non_null(id);

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.sub-no-id-2.", INT32_MAX, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    id = GETP_ARG(&ret, "0.0.ID");
    assert_non_null(id);

    remove_sub(alias_1);
    remove_sub(alias_2);
    amxc_var_clean(&ret);
}

void test_sub_id_should_use_provided_index(UNUSED void** state) {
    amxd_object_t* sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    amxd_trans_t trans;
    amxc_var_t ret;
    const char* id = NULL;
    const char* alias = "sub-index-100";

    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 100, alias);
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.unit-test.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.sub-index-100.", INT32_MAX, &ret, 5), 0);
    amxc_var_dump(&ret, STDOUT_FILENO);
    id = GETP_CHAR(&ret, "0.0.ID");
    assert_string_equal(id, "cpe-ID-100");

    remove_sub(alias);
    amxc_var_clean(&ret);
}

void test_add_subscription_valchange_object_with_time_to_live(UNUSED void** state) {
    amxd_object_t* sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    amxd_trans_t trans;
    amxc_var_t ret;
    amxc_var_t* get_response = NULL;
    int status;

    amxc_var_init(&ret);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 0, "sub_1_ttl_5");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "ID", "sub_1_ttl_5");
    amxd_trans_set_value(cstring_t, &trans, "NotifType", "ValueChange");
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.unit-test.");
    amxd_trans_set_value(cstring_t, &trans, "ReferenceList", "Device.Phonebook.Contact.1.");
    amxd_trans_set_value(uint32_t, &trans, "TimeToLive", 5);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    assert_int_equal(amxb_get(bus_ctx, "LocalAgent.Subscription.sub_1_ttl_5.", INT32_MAX, &ret, 5), 0);
    amxc_var_dump(&ret, 1);

    get_response = GETP_ARG(&ret, "0.0");
    amxc_var_dump(get_response, 1);
    assert_non_null(get_response);
    assert_false(amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, get_response)));
    assert_int_equal(strcmp("sub_1_ttl_5", GET_CHAR(get_response, "ID")), 0);
    assert_int_equal(GET_INT32(get_response, "TimeToLive"), 5);

    amxut_timer_go_to_future_ms(6000);
    amxp_timers_calculate();
    amxp_timers_check();
    handle_events();

    status = amxb_get(bus_ctx, "LocalAgent.Subscription.sub_1_ttl_5.", 0, &ret, 5);
    assert_int_equal(status, amxd_status_object_not_found);

    amxc_var_clean(&ret);
}

void test_update_subscription_valchange_object_with_time_to_live(UNUSED void** state) {
    amxd_object_t* sub_obj = amxd_dm_findf(&dm, "LocalAgent.Subscription.");
    amxd_trans_t trans;
    amxc_var_t ret;
    amxc_var_t args;
    amxc_var_t* get_response = NULL;
    int status;

    amxc_var_init(&ret);
    amxc_var_init(&args);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, sub_obj);
    amxd_trans_add_inst(&trans, 0, "sub_1_ttl_300");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "ID", "sub_1_ttl_300");
    amxd_trans_set_value(cstring_t, &trans, "NotifType", "ValueChange");
    amxd_trans_set_value(cstring_t, &trans, "Recipient", "LocalAgent.Controller.unit-test.");
    amxd_trans_set_value(cstring_t, &trans, "ReferenceList", "Device.Phonebook.Contact.1.");
    amxd_trans_set_value(uint32_t, &trans, "TimeToLive", 300);
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &args, "TimeToLive", 4);
    assert_int_equal(amxb_set(bus_ctx, "LocalAgent.Subscription.sub_1_ttl_300.", &args, &ret, 5), 0);

    handle_events();

    status = amxb_get(bus_ctx, "LocalAgent.Subscription.sub_1_ttl_300.", 0, &ret, 5);
    assert_int_equal(status, amxd_status_ok);
    assert_int_equal(GETP_UINT32(&ret, "0.0.TimeToLive"), 4);

    amxut_timer_go_to_future_ms(5000);
    amxp_timers_calculate();
    amxp_timers_check();
    handle_events();

    status = amxb_get(bus_ctx, "LocalAgent.Subscription.sub_1_ttl_300.", 0, &ret, 5);
    assert_int_equal(status, amxd_status_object_not_found);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);
}
