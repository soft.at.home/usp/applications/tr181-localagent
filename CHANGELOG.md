# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v3.0.1 - 2025-01-07(15:39:46 +0000)

### Other

- Notifications - Subscription Expiration is failing

## Release v3.0.0 - 2024-11-07(13:58:02 +0000)

### Other

- - Vendor prefix non standard parameters

## Release v2.7.6 - 2024-09-30(06:15:33 +0000)

### Other

- CI: Disable squashing of open source commits
- [AMX] Disable commit squashing

## Release v2.7.5 - 2024-08-02(20:20:39 +0000)

### Other

- - [amx] Failing to restart processes with init scripts

## Release v2.7.4 - 2024-05-16(11:54:42 +0000)

### Other

- [LocalAgent] define all key parameters as upc

## Release v2.7.3 - 2024-05-02(10:49:27 +0000)

### Other

- CLONE - LocalAgent doesn't start

## Release v2.7.2 - 2024-04-26(08:06:41 +0000)

### Fixes

- [CDROUTER][USP][REGRESSION] Cannot run USP tests on cdrouter due to the EndpointID new value

## Release v2.7.1 - 2024-04-18(14:55:48 +0000)

### Changes

- Make amxb timeouts configurable

## Release v2.7.0 - 2024-03-29(07:43:26 +0000)

### New

- [USP] Support all kind of "Cause" for Boot! Event

## Release v2.6.1 - 2024-03-18(11:45:44 +0000)

### Fixes

- [USP] Triggered! parameters need to be in a data variant

## Release v2.6.0 - 2024-02-15(09:50:12 +0000)

## Release v2.5.0 - 2024-02-05(16:18:26 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v2.4.6 - 2024-01-30(09:03:02 +0000)

### Other

- [USP][B&R] PeriodicNotifInterval  parameter is not upgrade persistent

## Release v2.4.5 - 2024-01-24(16:20:43 +0000)

### Other

- [USP] Remove sed from localagent startup script

## Release v2.4.4 - 2024-01-24(16:03:30 +0000)

### Other

- [BaRt] Failed to handle multiple keys

## Release v2.4.3 - 2024-01-22(10:54:33 +0000)

### Other

- [USP] Random String from EndpointId is not randomly enough

## Release v2.4.2 - 2024-01-22(07:33:36 +0000)

### Other

- [BaRt] Failed to handle multiple keys

## Release v2.4.1 - 2024-01-20(08:15:22 +0000)

### Other

- [USP] Random String from EndpointId is not randomly enough

## Release v2.4.0 - 2024-01-17(09:30:31 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v2.3.6 - 2023-12-07(10:42:37 +0000)

### Other

- [LocalAgent] Remove default MQTT client from odl file

## Release v2.3.5 - 2023-11-08(14:52:00 +0000)

### Other

- Refactor libamxo - libamxp: move fd and connection management out of libamxo

## Release v2.3.4 - 2023-10-27(09:31:18 +0000)

### Other

- [USP Agent] implement upgrade persistency for LocalAgent/MQTT Client

## Release v2.3.3 - 2023-10-25(13:37:37 +0000)

### Other

- [USP Agent] implement upgrade persistency for LocalAgent/MQTT Client

## Release v2.3.2 - 2023-09-28(10:35:56 +0000)

### Other

- Fix license headers in files

## Release v2.3.1 - 2023-09-28(07:55:17 +0000)

### Changes

- [USP] LocalAgent EndpointID should be reboot persistent

## Release v2.3.0 - 2023-09-07(16:22:58 +0000)

### Security

- [USP][ACL] A remote USP controller is only allowed to access TR-181 dms

## Release v2.2.2 - 2023-09-01(07:47:23 +0000)

### Fixes

- [USP] OnBoardingComplete must be persistent

## Release v2.2.1 - 2023-08-17(13:45:42 +0000)

### Changes

- Limit length to 50 and use .. separators for oui_ext

## Release v2.2.0 - 2023-08-17(13:08:06 +0000)

### New

- [USP] Add OnBoardingComplete parameter

## Release v2.1.0 - 2023-07-31(15:23:38 +0000)

### Other

- [USP] LocalAgent must wait for UnixDomainSockets plugin

## Release v2.0.2 - 2023-07-25(13:14:50 +0000)

### Other

- [USP] Update LocalAgent debug script

## Release v2.0.1 - 2023-07-14(14:34:14 +0000)

### Other

- Add SocketStatus parameter to data model

## Release v2.0.0 - 2023-07-11(11:17:58 +0000)

### Breaking

- [IMTP] Implement IMTP communication as specified in TR-369

## Release v1.12.0 - 2023-06-29(09:26:25 +0000)

### New

- [USP] Change USP EndpointId authority-scheme to "oui"

## Release v1.11.2 - 2023-06-29(09:12:39 +0000)

### Other

- - [KPN][USP] Boot! event is not as expected

## Release v1.11.1 - 2023-06-27(07:48:27 +0000)

### Other

- [CR9HF] - Multiple tr181-localagent sessions running
- EnableMDNS is true by default in spec

## Release v1.11.0 - 2023-06-23(08:27:53 +0000)

### New

- [USP] Trigger 'Sendonboardrequest()' via TR-069/TR-369 Parameter

### Other

- - [HTTPManager][WebUI] Create plugin's ACLs permissions

## Release v1.10.1 - 2023-05-26(07:27:38 +0000)

### Fixes

- [USP][MQTT] Missing unique keys for MQTT data model

## Release v1.10.0 - 2023-05-11(07:17:08 +0000)

### New

- [USP] It must be possible to use protected methods

## Release v1.9.4 - 2023-05-04(09:56:27 +0000)

### Other

- [USP] Add missing event parameters to data model

## Release v1.9.3 - 2023-05-02(13:52:01 +0000)

### Fixes

- [USP] tr181-localagent should use amxp_dir_owned_make

## Release v1.9.2 - 2023-04-27(08:06:39 +0000)

### Other

- Also update Alias

## Release v1.9.1 - 2023-04-27(07:14:49 +0000)

### Other

- Extend defaults with Controller MTP

## Release v1.9.0 - 2023-04-26(07:17:15 +0000)

### New

- Issue NET-4657: [USP] Add NotifType AmxNotification for ambiorix events

## Release v1.8.2 - 2023-04-13(12:57:07 +0000)

### Fixes

- [USP] Stopping tr181-mqtt before tr181-localagent resets references

## Release v1.8.1 - 2023-04-12(07:29:58 +0000)

### Changes

- Move AutoReconnect to LocalAgent.MTP.i.MQTT.

## Release v1.8.0 - 2023-04-05(10:44:17 +0000)

### New

- USP agent must know when to ForceReconnect() client

## Release v1.7.6 - 2023-04-04(11:30:48 +0000)

### Fixes

- [USP] Incorrect parameter attributes

## Release v1.7.5 - 2023-04-03(07:56:29 +0000)

### Fixes

- [USP] Lists of strings must be of type csv_string_t

## Release v1.7.4 - 2023-03-21(09:42:05 +0000)

### Fixes

- [KPN][USP] Boot! event notification is not as expected

## Release v1.7.3 - 2023-03-10(10:32:44 +0000)

### Fixes

- Let uspagent save LastValue for parameter subscriptions

## Release v1.7.2 - 2023-02-16(10:30:54 +0000)

### Other

- Add tr181-mqtt/tr181-localagent/uspagent into processmonitor
- Create direct pcb connection to tr181-mqtt

## Release v1.7.1 - 2023-02-14(10:49:37 +0000)

### Fixes

- [USP] Creation of LocalAgent.Subscription from save file can fail

## Release v1.7.0 - 2023-02-13(17:29:09 +0000)

### New

- Include advertisement module in localagent

## Release v1.6.10 - 2023-02-13(10:43:56 +0000)

### Fixes

- Box12 doesn't onboard in Motive

## Release v1.6.9 - 2023-01-09(10:29:37 +0000)

### Other

- [KPN SW2][Security]Restrict ACL of admin user

## Release v1.6.8 - 2022-12-09(10:28:00 +0000)

### Fixes

- [USP] Triggered event needs an exclamation mark

## Release v1.6.7 - 2022-11-24(09:17:41 +0000)

### Fixes

- [AMX] Apply new amxd_path_setf formatting

## Release v1.6.6 - 2022-11-24(07:50:55 +0000)

### Other

- Add extra controller instance

## Release v1.6.5 - 2022-11-23(11:54:23 +0000)

### Other

- [USP] Add IMTP defaults to tr181-localagent

## Release v1.6.4 - 2022-11-10(11:37:11 +0000)

### Fixes

- Missing Periodic! event in data model

## Release v1.6.3 - 2022-10-18(12:41:10 +0000)

### Fixes

- [USP] Permission denied due to ControllerTrust mismatch

## Release v1.6.2 - 2022-10-17(10:59:16 +0000)

### Fixes

- [USP] Values of reference parameters should start with Device. (revert)

## Release v1.6.1 - 2022-10-14(13:32:51 +0000)

### Fixes

- [USP] Values of reference parameters should start with Device.

## Release v1.6.0 - 2022-10-11(15:14:12 +0000)

### New

- Add method to get controller MTP info from dm

## Release v1.5.4 - 2022-10-10(10:07:26 +0000)

### Fixes

- AssignedRole parameter must have a Device. prefix

## Release v1.5.3 - 2022-10-07(13:38:53 +0000)

### Fixes

- Missing MTPNumberOfEntries parameter

## Release v1.5.2 - 2022-09-15(11:34:57 +0000)

### Fixes

- [USP] Agent must assign default values

## Release v1.5.1 - 2022-09-13(14:39:40 +0000)

### Fixes

- [USP] [Add_Msg] The AddResp does not contain the elements in the unique keymap under the OperationSuccess

## Release v1.5.0 - 2022-08-29(14:02:04 +0000)

### New

- Add MTP of type IMTP

## Release v1.4.2 - 2022-08-25(08:21:12 +0000)

### Fixes

- PeriodicNotifInterval value should be at least 1

## Release v1.4.1 - 2022-08-16(11:18:33 +0000)

### Fixes

- [ACS][V12] RG does not send a Boot! event for the second Reboot() command

## Release v1.4.0 - 2022-08-03(11:17:10 +0000)

### New

- Value change on Device.DeviceInfo.SoftwareVersion after firmware upgrade

## Release v1.3.1 - 2022-07-14(13:08:17 +0000)

### Changes

- [USP] A dot must be added to reference paths

## Release v1.3.0 - 2022-06-28(14:09:43 +0000)

### New

- [USP][KPN] Set ResponseTopicConfigured to hdm topic

## Release v1.2.2 - 2022-06-09(06:56:19 +0000)

### Fixes

- Problem with loading defaults file

## Release v1.2.1 - 2022-06-08(14:05:42 +0000)

### Fixes

- [USP] The USP agent should store its persistent parameters periodically

## Release v1.2.0 - 2022-06-08(06:25:15 +0000)

### New

- [USP Agent] Implement TransferComplete functionality

## Release v1.1.8 - 2022-06-02(13:18:59 +0000)

### Other

- Component should have a debian package
- Issue: amx/usp/applications/tr181-localagent#1 Component should have a debian package

## Release v1.1.7 - 2022-05-20(14:41:24 +0000)

### Other

- Add libamxj to deps
- [USPAgent] The USP Agent must be opensourced to gitlab.com/soft.at.home

## Release v1.1.6 - 2022-05-19(12:47:09 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v1.1.5 - 2022-05-04(06:15:02 +0000)

### Fixes

- Correctly install odl files

## Release v1.1.4 - 2022-04-29(14:40:42 +0000)

### Fixes

- [MQTT] Clients connecting with the same ID cause conflicts

## Release v1.1.3 - 2022-04-21(11:35:29 +0000)

### Fixes

- [USP] LocalAgent.Subscription is no longer persistent

## Release v1.1.2 - 2022-04-19(06:10:34 +0000)

### Fixes

- [USP][MQTT] Update order of entrypoints

## Release v1.1.1 - 2022-04-14(09:49:16 +0000)

### Fixes

- [USP] It must be possible to override odl files

## Release v1.1.0 - 2022-04-12(13:05:22 +0000)

### New

- [USP][MQTT] MQTT topic needs to be set dynamically

## Release v1.0.1 - 2022-04-05(06:06:31 +0000)

### Fixes

- [Device] Support Sending Boot! event

## Release v1.0.0 - 2022-03-28(15:09:27 +0000)

### Breaking

- [USPAgent] Split agent in datamodel part and function part

### Changes

- [USP] Re-enable data model persistency in uspagent and tr181-mqtt

