include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)-storage.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)-storage.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_software_version.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_software_version.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_extra.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_extra.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-localagent_mapping.odl
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(DEST)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/defaults
	$(INSTALL) -D -p -m 0644 odl/defaults/* $(DEST)/etc/amx/$(COMPONENT)/defaults/
	$(INSTALL) -d -m 0755 $(DEST)$(PROCMONDIR)
	ln -sfr $(DEST)$(INITDIR)/$(COMPONENT) $(DEST)$(PROCMONDIR)/$(COMPONENT)
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)

package: all
	$(INSTALL) -D -p -m 0755 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)-storage.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)-storage.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_software_version.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_software_version.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_extra.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_extra.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-localagent_mapping.odl
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/defaults
	$(INSTALL) -D -p -m 0644 odl/defaults/* $(PKGDIR)/etc/amx/$(COMPONENT)/defaults/
	$(INSTALL) -d -m 0755 $(PKGDIR)$(PROCMONDIR)
	rm -f $(PKGDIR)$(PROCMONDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(INITDIR)/$(COMPONENT) $(PKGDIR)$(PROCMONDIR)/$(COMPONENT)
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package test