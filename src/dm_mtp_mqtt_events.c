/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>

#include "dm_mtp_mqtt_events.h"

static bool mqtt_subscription_exists(const char* client, const char* topic) {
    bool retval = false;
    amxb_bus_ctx_t* bus_ctx = agent_get_busctxt();
    amxc_var_t ret;
    amxc_string_t path;

    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    amxc_string_setf(&path, "%s.Subscription.[Topic=='%s'].", client, topic);
    amxb_get(bus_ctx, amxc_string_get(&path, 0), 0, &ret, 5);

    if(GETP_ARG(&ret, "0.0") != NULL) {
        retval = true;
    }

    amxc_var_clean(&ret);
    amxc_string_clean(&path);
    return retval;
}

static int mqtt_add_subscription(const char* client, const char* topic) {
    int retval = -1;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t subscription;
    amxb_bus_ctx_t* bus_ctx = agent_get_busctxt();

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_string_init(&subscription, 0);

    if(mqtt_subscription_exists(client, topic)) {
        retval = 0;
        goto exit;
    }
    amxc_string_setf(&subscription, "%sSubscription.", client);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Topic", topic);
    amxc_var_add_key(bool, &args, "Enable", true);

    retval = amxb_add(bus_ctx, amxc_string_get(&subscription, 0), 0, NULL, &args, &ret, 5);
    SAH_TRACEZ_INFO(ME, "Add MQTT subscription for topic %s returned %d", topic, retval);
    when_failed(retval, exit);

exit:
    amxc_string_clean(&subscription);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return retval;
}

static int mqtt_del_subscription(const char* client, const char* topic) {
    int retval = -1;
    amxc_var_t ret;
    amxc_string_t subscription;
    amxb_bus_ctx_t* bus_ctx = agent_get_busctxt();

    amxc_var_init(&ret);
    amxc_string_init(&subscription, 0);

    if(topic == NULL) {
        amxc_string_setf(&subscription, "%sSubscription.*.", client);
        SAH_TRACEZ_INFO(ME, "Deleting all subscriptions");
    } else {
        amxc_string_setf(&subscription, "%sSubscription.[Topic == '%s'].", client, topic);
        SAH_TRACEZ_INFO(ME, "Deleting subscription %s", amxc_string_get(&subscription, 0));
    }
    retval = amxb_del(bus_ctx, amxc_string_get(&subscription, 0), 0, NULL, &ret, 5);
    when_failed(retval, exit);

exit:
    amxc_string_clean(&subscription);
    amxc_var_clean(&ret);
    return retval;
}

void _mtp_mqtt_update_subs(UNUSED const char* const sig_name,
                           const amxc_var_t* const data,
                           UNUSED void* const priv) {
    amxd_object_t* mqtt_mtp = amxd_dm_signal_get_object(agent_get_dm(), data);
    char* client = amxd_object_get_value(cstring_t, mqtt_mtp, "Reference", NULL);
    const char* from = GETP_CHAR(data, "parameters.ResponseTopicConfigured.from");
    const char* to = GETP_CHAR(data, "parameters.ResponseTopicConfigured.to");
    amxd_path_t client_d;

    amxd_path_init(&client_d, NULL);

    when_str_empty(client, exit);
    amxd_path_setf(&client_d, true, "%s", client);

    SAH_TRACEZ_INFO(ME, "ResponseTopicConfigured has changed - update MQTT subscription");
    SAH_TRACEZ_INFO(ME, "Changed from %s to %s for client %s", from, to,
                    amxd_path_get(&client_d, AMXD_OBJECT_TERMINATE));
    mqtt_del_subscription(amxd_path_get(&client_d, AMXD_OBJECT_TERMINATE), from);
    mqtt_add_subscription(amxd_path_get(&client_d, AMXD_OBJECT_TERMINATE), to);

exit:
    amxd_path_clean(&client_d);
    free(client);
}
