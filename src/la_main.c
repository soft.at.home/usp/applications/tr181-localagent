/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>

#include <debug/sahtrace.h>

#include "localagent.h"
#include "dm_agent.h"
#include "dm_controllertrust.h"
#include "dm_subscription.h"
#include "dm_monitor.h"

static amxd_dm_t* datamodel = NULL;
static amxo_parser_t* plugin_parser = NULL;
static amxb_bus_ctx_t* plugin_busctxt = NULL;

static void mqtt_con_read(int fd, void* priv) {
    amxb_bus_ctx_t* ctx = (amxb_bus_ctx_t*) priv;

    if(amxb_read(ctx) != 0) {
        SAH_TRACEZ_WARNING(ME, "Failed to read from tr181-mqtt connection, did the remote end hang up?");
        amxo_connection_remove(plugin_parser, fd);
        amxb_free(&ctx);
    }
}

static void agent_init_pre_odl(amxo_parser_t* parser) {
    amxb_bus_ctx_t* mqtt_ctx = NULL;
    int retval = -1;

    plugin_busctxt = amxb_be_who_has("LocalAgent");

    retval = amxb_connect(&mqtt_ctx, "pcb:/var/run/mqtt/tr181-mqtt");
    if(retval == 0) {
        SAH_TRACEZ_INFO(ME, "Created direct pcb connection to tr181-mqtt");
        amxo_connection_add(parser, amxb_get_fd(mqtt_ctx), mqtt_con_read, NULL, AMXO_BUS, mqtt_ctx);
    } else {
        SAH_TRACEZ_WARNING(ME, "Failed to connect directly to tr181-mqtt (expected if pcb is not used)");
    }
}

static void agent_init_post_odl(UNUSED amxd_dm_t* dm,
                                amxo_parser_t* parser) {
    SAH_TRACEZ_INFO(ME, "tr181-localagent started");

    dm_agent_set_endpoint_id(parser);
    dm_agent_start_uptime();
    dm_controllertrust_init();
    dm_subscription_init();
    dm_monitor_init();

}

static void agent_cleanup(void) {
    SAH_TRACEZ_INFO(ME, "tr181-localagent stopped");
    datamodel = NULL;
    plugin_parser = NULL;
    dm_monitor_clean();
    return;
}

// Debugging - print events - activate in odl
void _print_event(const char* const sig_name,
                  const amxc_var_t* const data,
                  UNUSED void* const priv) {
    printf("event received - %s\n", sig_name);
    if(data != NULL) {
        printf("Event data = \n");
        fflush(stdout);
        amxc_var_dump(data, STDOUT_FILENO);
    }
}

amxd_dm_t* agent_get_dm(void) {
    return datamodel;
}

amxb_bus_ctx_t* agent_get_busctxt(void) {
    return plugin_busctxt;
}

amxo_parser_t* agent_get_parser(void) {
    return plugin_parser;
}

int _la_main(int reason,
             amxd_dm_t* dm,
             amxo_parser_t* parser) {
    switch(reason) {
    case 0:     // START
        datamodel = dm;
        plugin_parser = parser;
        agent_init_pre_odl(parser);
        break;
    case 1:     // STOP
        agent_cleanup();
        break;
    case 2:     // ODL_LOADED
        agent_init_post_odl(dm, parser);
        break;
    }
    return 0;
}
