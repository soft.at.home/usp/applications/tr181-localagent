/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "dm_controllertrust.h"

static int controllertrust_create_dir(const char* dir_name) {
    struct stat st = {0};
    int retval = 0;

    if(stat(dir_name, &st) == -1) {
        retval = amxp_dir_owned_make(dir_name,
                                     AMXA_DIR_PERMISSIONS,
                                     amxa_utils_get_owner_uid(),
                                     amxa_utils_get_group_gid());
        if(retval != 0) {
            SAH_TRACEZ_WARNING(ME, "Error creating dir: %s", strerror(errno));
        }
    }

    return retval;
}

static void controllertrust_write_to_file(const char* file_name, amxc_var_t* request) {
    variant_json_t* writer = NULL;
    int fd = 0;

    amxj_writer_new(&writer, request);

    fd = open(file_name, O_RDWR | O_CREAT, 0600);
    if(fd == -1) {
        SAH_TRACEZ_ERROR(ME, "Failed to open ACL file: %s", file_name);
        goto exit;
    }

    amxj_write(writer, fd);

exit:
    amxj_writer_delete(&writer);
}

static void controllertrust_write_acl(amxd_object_t* permission, const char* file_name) {
    amxc_var_t params;
    amxc_var_t request;
    amxc_var_t* ref_list = NULL;

    amxc_var_init(&params);
    amxc_var_init(&request);

    amxd_object_get_params(permission, &params, amxd_dm_access_protected);
    ref_list = GETP_ARG(&params, "Targets");

    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_cast(ref_list, AMXC_VAR_ID_LIST);
    amxc_var_for_each(ref_path, ref_list) {
        const char* target = amxc_var_constcast(cstring_t, ref_path);
        amxc_var_t* param = amxc_var_add_key(amxc_htable_t, &request, target, NULL);
        amxc_var_add_key(cstring_t, param, "CommandEvent", GET_CHAR(&params, "CommandEvent"));
        amxc_var_add_key(cstring_t, param, "InstantiatedObj", GET_CHAR(&params, "InstantiatedObj"));
        amxc_var_add_key(cstring_t, param, "Obj", GET_CHAR(&params, "Obj"));
        amxc_var_add_key(uint32_t, param, "Order", GET_UINT32(&params, "Order"));
        amxc_var_add_key(cstring_t, param, "Param", GET_CHAR(&params, "Param"));
    }
    controllertrust_write_to_file(file_name, &request);

    amxc_var_clean(&params);
    amxc_var_clean(&request);
}

static void permissions_init(amxd_object_t* role_instance, const char* dir_name) {
    amxd_object_t* permission_object = amxd_object_findf(role_instance, ".Permission");
    amxc_string_t file_name;
    amxc_string_init(&file_name, 0);

    amxd_object_for_each(instance, it, permission_object) {
        amxd_object_t* permission_instance = amxc_container_of(it, amxd_object_t, it);
        amxc_var_t params;
        bool enable = false;
        const char* alias = NULL;

        amxc_var_init(&params);
        amxd_object_get_params(permission_instance, &params, amxd_dm_access_protected);
        enable = GET_BOOL(&params, "Enable");
        if(enable == false) {
            amxc_var_clean(&params);
            continue;
        }

        alias = GET_CHAR(&params, "Alias");

        amxc_string_setf(&file_name, "%s%s.json", dir_name, alias);
        controllertrust_write_acl(permission_instance, amxc_string_get(&file_name, 0));

        amxc_var_clean(&params);
    }
    amxc_string_clean(&file_name);
}

void _Role_Added(UNUSED const char* const sig_name,
                 const amxc_var_t* const data,
                 UNUSED void* const priv) {
    amxd_dm_t* dm = agent_get_dm();
    amxo_parser_t* parser = agent_get_parser();
    const char* alias = GETP_CHAR(data, "parameters.Alias");
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* role_object = amxd_dm_signal_get_object(dm, data);
    amxd_object_t* role_instance = amxd_object_get_instance(role_object, alias, 0);
    amxc_string_t dir_name;
    char* role_name = NULL;

    amxc_string_init(&dir_name, 0);

    when_null(parser, exit);

    role_name = amxd_object_get_cstring_t(role_instance, "Name", &status);
    SAH_TRACEZ_INFO(ME, "New role added: %s", role_name);
    amxc_string_setf(&dir_name, "%s/%s/", GET_CHAR(&parser->config, "acl_dir"), role_name);
    controllertrust_create_dir(amxc_string_get(&dir_name, 0));

exit:
    free(role_name);
    amxc_string_clean(&dir_name);
}

void _Role_Changed(UNUSED const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    amxo_parser_t* parser = agent_get_parser();
    const char* old_name = GETP_CHAR(data, "parameters.Name.from");
    const char* new_name = GETP_CHAR(data, "parameters.Name.to");
    amxc_string_t old_dir;
    amxc_string_t new_dir;

    amxc_string_init(&old_dir, 0);
    amxc_string_init(&new_dir, 0);

    when_str_empty(old_name, exit);
    when_str_empty(new_name, exit);

    amxc_string_setf(&old_dir, "%s/%s/", GET_CHAR(&parser->config, "acl_dir"), old_name);
    amxc_string_setf(&new_dir, "%s/%s/", GET_CHAR(&parser->config, "acl_dir"), new_name);

    if(rename(amxc_string_get(&old_dir, 0), amxc_string_get(&new_dir, 0)) != 0) {
        SAH_TRACEZ_WARNING(ME, "Renaming Role failed");
    }

exit:
    amxc_string_clean(&old_dir);
    amxc_string_clean(&new_dir);
    return;
}

void _Permission_Added(UNUSED const char* const sig_name,
                       const amxc_var_t* const data,
                       UNUSED void* const priv) {
    amxd_object_t* permission_object = amxd_dm_signal_get_object(agent_get_dm(), data);
    amxd_object_t* role_object = NULL;
    amxo_parser_t* parser = agent_get_parser();
    char* role_name = NULL;
    const char* alias = GETP_CHAR(data, "parameters.Alias");
    amxc_var_t* ref_list = GETP_ARG(data, "parameters.Targets");
    bool enable = GETP_BOOL(data, "parameters.Enable");
    amxc_string_t file_name;
    amxc_var_t request;

    SAH_TRACEZ_INFO(ME, "New permission added: %s", alias);
    amxc_string_init(&file_name, 0);
    amxc_var_init(&request);

    when_null(parser, exit);
    when_false(enable, exit);

    role_object = amxd_object_get_parent(permission_object);
    role_name = amxd_object_get_value(cstring_t, role_object, "Name", NULL);
    amxc_string_setf(&file_name, "%s/%s/%s.json", GET_CHAR(&parser->config, "acl_dir"), role_name, alias);

    amxc_var_set_type(&request, AMXC_VAR_ID_HTABLE);
    amxc_var_cast(ref_list, AMXC_VAR_ID_LIST);
    amxc_var_for_each(ref_path, ref_list) {
        const char* target = amxc_var_constcast(cstring_t, ref_path);
        amxc_var_t* param = amxc_var_add_key(amxc_htable_t, &request, target, NULL);
        amxc_var_add_key(cstring_t, param, "CommandEvent", GETP_CHAR(data, "parameters.CommandEvent"));
        amxc_var_add_key(cstring_t, param, "InstantiatedObj", GETP_CHAR(data, "parameters.InstantiatedObj"));
        amxc_var_add_key(cstring_t, param, "Obj", GETP_CHAR(data, "parameters.Obj"));
        amxc_var_add_key(uint32_t, param, "Order", GETP_UINT32(data, "parameters.Order"));
        amxc_var_add_key(cstring_t, param, "Param", GETP_CHAR(data, "parameters.Param"));
    }
    controllertrust_write_to_file(amxc_string_get(&file_name, 0), &request);

    free(role_name);
    amxc_var_clean(&request);
    amxc_string_clean(&file_name);
exit:
    return;
}

void _Permission_Changed(UNUSED const char* const sig_name,
                         const amxc_var_t* const data,
                         UNUSED void* const priv) {
    amxo_parser_t* parser = agent_get_parser();
    amxd_object_t* permission_object = amxd_dm_signal_get_object(agent_get_dm(), data);
    amxd_object_t* role_object = amxd_object_get_parent(amxd_object_get_parent(permission_object));
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t* enable_var = GETP_ARG(data, "parameters.Enable.to");
    amxc_string_t file_string;
    const char* file_name = NULL;
    char* role_name = amxd_object_get_value(cstring_t, role_object, "Name", &status);
    const char* alias = amxd_object_get_name(permission_object, AMXD_OBJECT_NAMED);

    amxc_string_init(&file_string, 0);
    amxc_string_setf(&file_string, "%s/%s/%s.json", GET_CHAR(&parser->config, "acl_dir"), role_name, alias);
    file_name = amxc_string_get(&file_string, 0);

    if(enable_var == NULL) {
        if(amxd_object_get_bool(permission_object, "Enable", &status)) {
            controllertrust_write_acl(permission_object, file_name);
        } else {
            goto exit;
        }
    } else {
        bool enable = amxc_var_constcast(bool, enable_var);
        if(enable) {
            controllertrust_write_acl(permission_object, file_name);
        } else {
            remove(file_name);
        }
    }

exit:
    amxc_string_clean(&file_string);
    return;
}

void dm_controllertrust_init(void) {
    amxd_dm_t* dm = agent_get_dm();
    amxd_object_t* role_object = amxd_dm_findf(dm, "LocalAgent.ControllerTrust.Role");
    amxc_string_t dir_name;
    amxc_string_init(&dir_name, 0);

    amxd_object_for_each(instance, it, role_object) {
        amxd_object_t* role_instance = amxc_container_of(it, amxd_object_t, it);
        char* role_name = amxd_object_get_value(cstring_t, role_instance, "Name", NULL);
        amxo_parser_t* parser = agent_get_parser();

        amxc_string_setf(&dir_name, "%s/%s/", GET_CHAR(&parser->config, "acl_dir"), role_name);
        SAH_TRACEZ_INFO(ME, "New role added: %s", role_name);

        if(controllertrust_create_dir(amxc_string_get(&dir_name, 0)) != 0) {
            free(role_name);
            goto exit;
        }

        permissions_init(role_instance, amxc_string_get(&dir_name, 0));
        free(role_name);
    }

exit:
    amxc_string_clean(&dir_name);
    return;
}
