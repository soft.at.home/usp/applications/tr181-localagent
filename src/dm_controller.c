/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "dm_controller.h"

static amxd_status_t reset_onboarding_complete(amxd_object_t* object) {
    amxd_status_t retval = amxd_status_unknown_error;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "OnBoardingComplete", false);
    retval = amxd_trans_apply(&trans, agent_get_dm());

    amxd_trans_clean(&trans);
    return retval;
}

amxd_status_t _Controller_SendOnBoardRequest(amxd_object_t* object,
                                             UNUSED amxd_function_t* func,
                                             UNUSED amxc_var_t* args,
                                             UNUSED amxc_var_t* ret) {
    int retval = -1;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t rv;
    amxc_var_t event;
    amxc_var_t* dev_info = NULL;
    amxd_object_t* agent_obj = amxd_dm_findf(agent_get_dm(), "LocalAgent.");
    char* agent_eid = NULL;
    char* contr_eid = NULL;

    amxc_var_init(&rv);
    amxc_var_init(&event);
    amxc_var_set_type(&event, AMXC_VAR_ID_HTABLE);

    retval = amxb_get(agent_get_busctxt(), "DeviceInfo.", 0, &rv, 5);
    when_failed(retval, exit);
    dev_info = GETP_ARG(&rv, "0.'DeviceInfo.'");

    amxc_var_add_key(cstring_t, &event, "oui", GET_CHAR(dev_info, "ManufacturerOUI"));
    amxc_var_add_key(cstring_t, &event, "product_class", GET_CHAR(dev_info, "ProductClass"));
    amxc_var_add_key(cstring_t, &event, "serial_number", GET_CHAR(dev_info, "SerialNumber"));
    amxc_var_add_key(cstring_t, &event, "agent_supported_protocol_version", AGENT_SUPPORTED_PROTOCOL_VERSIONS);

    agent_eid = amxd_object_get_value(cstring_t, agent_obj, "EndpointID", NULL);
    amxc_var_add_key(cstring_t, &event, "agent_eid", agent_eid);
    contr_eid = amxd_object_get_value(cstring_t, object, "EndpointID", NULL);
    amxc_var_add_key(cstring_t, &event, "contr_eid", contr_eid);

    SAH_TRACEZ_INFO(ME, "OnBoardRequest! for controller: %s", contr_eid);
    amxd_object_send_signal(object, "OnBoardRequest!", &event, false);
    reset_onboarding_complete(object);

    status = amxd_status_ok;
exit:
    free(agent_eid);
    free(contr_eid);
    amxc_var_clean(&rv);
    amxc_var_clean(&event);
    return status;
}

amxd_status_t _Controller_SendOnBoardRequest_CLI(amxd_object_t* object,
                                                 UNUSED amxd_function_t* func,
                                                 amxc_var_t* args,
                                                 UNUSED amxc_var_t* ret) {
    amxc_var_t event;
    amxd_object_t* agent_obj = NULL;
    char* agent_eid = NULL;
    char* contr_eid = NULL;
    char* oui = amxc_var_dyncast(cstring_t, GET_ARG(args, "oui"));
    char* product_class = amxc_var_dyncast(cstring_t, GET_ARG(args, "product_class"));
    char* serial_number = amxc_var_dyncast(cstring_t, GET_ARG(args, "serial_number"));
    char* aspv = amxc_var_dyncast(cstring_t, GET_ARG(args, "aspv"));

    amxc_var_init(&event);
    amxc_var_set_type(&event, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &event, "oui", oui);
    amxc_var_add_key(cstring_t, &event, "product_class", product_class);
    amxc_var_add_key(cstring_t, &event, "serial_number", serial_number);
    amxc_var_add_key(cstring_t, &event, "agent_supported_protocol_version", aspv);

    agent_obj = amxd_dm_findf(agent_get_dm(), "LocalAgent.");
    agent_eid = amxd_object_get_value(cstring_t, agent_obj, "EndpointID", NULL);
    amxc_var_add_key(cstring_t, &event, "agent_eid", agent_eid);
    contr_eid = amxd_object_get_value(cstring_t, object, "EndpointID", NULL);
    amxc_var_add_key(cstring_t, &event, "contr_eid", contr_eid);

    SAH_TRACEZ_INFO(ME, "OnBoardRequest! for controller: %s", contr_eid);
    amxd_object_send_signal(object, "OnBoardRequest!", &event, false);
    reset_onboarding_complete(object);

    free(aspv);
    free(serial_number);
    free(product_class);
    free(oui);
    free(agent_eid);
    free(contr_eid);
    amxc_var_clean(&event);
    return amxd_status_ok;
}

void _controller_send_on_board_request(UNUSED const char* const sig_name,
                                       const amxc_var_t* const data,
                                       UNUSED void* const priv) {
    amxd_dm_t* dm = agent_get_dm();
    amxo_parser_t* parser = agent_get_parser();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    const char* prefix = GET_CHAR(&parser->config, "prefix_");
    amxd_trans_t trans;
    amxc_string_t param_name;

    amxd_trans_init(&trans);
    amxc_string_init(&param_name, 0);
    amxc_string_setf(&param_name, "%sSendOnBoardRequest", prefix == NULL ? "" : prefix);

    SAH_TRACEZ_INFO(ME, "LocalAgent.Controller.%s.%s triggered",
                    amxd_object_get_name(object, 0), amxc_string_get(&param_name, 0));
    _Controller_SendOnBoardRequest(object, NULL, NULL, NULL);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, amxc_string_get(&param_name, 0), false);
    amxd_trans_apply(&trans, dm);

    amxd_trans_clean(&trans);
    amxc_string_clean(&param_name);
}

amxd_status_t _Controller_GetMTPInfo(amxd_object_t* object,
                                     UNUSED amxd_function_t* func,
                                     UNUSED amxc_var_t* args,
                                     amxc_var_t* ret) {
    amxd_object_t* mtp = amxd_object_findf(object, "MTP");
    amxd_object_t* mtp_instance = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    char* protocol = NULL;

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);

    amxd_object_for_each(instance, it, mtp) {
        bool enabled = false;

        mtp_instance = amxc_container_of(it, amxd_object_t, it);
        enabled = amxd_object_get_value(bool, mtp_instance, "Enable", NULL);
        if(!enabled) {
            continue;
        }

        protocol = amxd_object_get_value(cstring_t, mtp_instance, "Protocol", NULL);
        amxc_var_add_key(cstring_t, ret, "protocol", protocol);
        break;
    }

    if(protocol == NULL) {
        char* path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
        SAH_TRACEZ_WARNING(ME, "Unable to find enabled MTP for controller %s", path);
        free(path);
        goto exit;
    }

    if(strcmp(protocol, "MQTT") == 0) {
        amxd_object_t* mqtt = amxd_object_findf(mtp_instance, "MQTT");
        char* topic = amxd_object_get_value(cstring_t, mqtt, "Topic", NULL);
        amxc_var_add_key(cstring_t, ret, "topic", topic);
        free(topic);
    }

    status = amxd_status_ok;
exit:
    free(protocol);
    return status;
}
