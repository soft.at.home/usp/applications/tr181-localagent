/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <sys/sysinfo.h>
#include <errno.h>

#include "dm_agent.h"

typedef int (* create_endpoint_id_t) (amxc_string_t*);

typedef struct _get_endpoint_id {
    const char* authority_scheme;
    create_endpoint_id_t fn;
} get_endpoint_id_t;

static amxc_ts_t start_time;

static char* get_random_str(int length) {
    FILE* urandom = NULL;
    size_t read = 0;
    unsigned int seed;
    char* rand_str = (char*) calloc(1, length + 1);
    char* ptr = rand_str;

    when_null(rand_str, exit);

    urandom = fopen("/dev/urandom", "r");
    read = fread(&seed, sizeof(int), 1, urandom);
    if(read == 0) {
        SAH_TRACEZ_ERROR(ME, "fread error: %s", strerror(errno));
        fclose(urandom);
        goto exit;
    }
    fclose(urandom);

    srandom(seed);

    for(int i = 0; i < length; i++) {
        int random_number = random() % 62;
        if(random_number < 10) {
            *ptr = '0' + random_number;         // Convert to a digit (0-9)
        } else if(random_number < 36) {
            *ptr = 'A' + random_number - 10;    // Convert to uppercase letter (A-Z)
        } else {
            *ptr = 'a' + random_number - 36;    // Convert to lowercase letter (a-z)
        }
        ptr++;
    }

exit:
    return rand_str;
}

static int get_oui_id(amxc_string_t* endpoint_id) {
    int status = -1;
    amxc_var_t devinfo;
    amxc_var_t* infovar = NULL;
    const char* man_oui = NULL;
    const char* serial_nr = NULL;

    amxc_var_init(&devinfo);

    amxb_get(agent_get_busctxt(), "DeviceInfo.", 0, &devinfo, 5);
    infovar = GETP_ARG(&devinfo, "0.'DeviceInfo.'");
    when_null(infovar, exit);
    man_oui = GET_CHAR(infovar, "ManufacturerOUI");
    when_str_empty(man_oui, exit)
    serial_nr = GET_CHAR(infovar, "SerialNumber");
    when_str_empty(serial_nr, exit)

    status = amxc_string_setf(endpoint_id, "oui:%s:%s", man_oui, serial_nr);
exit:
    amxc_var_clean(&devinfo);
    return status;
}

static int get_oui_ext_id(amxc_string_t* endpoint_id) {
    int status = -1;
    amxc_var_t devinfo;
    amxc_var_t* infovar = NULL;
    const char* man_oui = NULL;
    const char* serial_nr = NULL;
    const char* prod_class = NULL;
    char* random_str = NULL;

    amxc_var_init(&devinfo);

    amxb_get(agent_get_busctxt(), "DeviceInfo.", 0, &devinfo, 5);
    infovar = GETP_ARG(&devinfo, "0.'DeviceInfo.'");
    when_null(infovar, exit);
    man_oui = GET_CHAR(infovar, "ManufacturerOUI");
    when_str_empty(man_oui, exit)
    prod_class = GET_CHAR(infovar, "ProductClass");
    when_str_empty(prod_class, exit)
    serial_nr = GET_CHAR(infovar, "SerialNumber");
    when_str_empty(serial_nr, exit)

    amxc_string_setf(endpoint_id, "oui:%s:%s..%s..", man_oui, serial_nr, prod_class);
    random_str = get_random_str(50 - amxc_string_text_length(endpoint_id));
    status = amxc_string_appendf(endpoint_id, "%s", random_str);

exit:
    free(random_str);
    amxc_var_clean(&devinfo);
    return status;
}

static int get_ops_id(amxc_string_t* endpoint_id) {
    int status = -1;
    amxc_var_t devinfo;
    amxc_var_t* infovar = NULL;
    const char* man_oui = NULL;
    const char* prod_class = NULL;
    const char* serial_nr = NULL;

    amxc_var_init(&devinfo);
    amxb_get(agent_get_busctxt(), "DeviceInfo.", 0, &devinfo, 5);
    infovar = GETP_ARG(&devinfo, "0.'DeviceInfo.'");
    when_null(infovar, exit);
    man_oui = GET_CHAR(infovar, "ManufacturerOUI");
    when_str_empty(man_oui, exit)
    prod_class = GET_CHAR(infovar, "ProductClass");
    when_str_empty(prod_class, exit)
    serial_nr = GET_CHAR(infovar, "SerialNumber");
    when_str_empty(serial_nr, exit)

    status = amxc_string_setf(endpoint_id, "ops::%s-%s-%s", man_oui, prod_class, serial_nr);
exit:
    amxc_var_clean(&devinfo);
    return status;
}

static int get_proto_id(amxc_string_t* endpoint_id) {
    char hostname[1024] = "";
    gethostname(hostname, 1023);
    amxc_string_setf(endpoint_id, "proto::Agent-%s", hostname);
    return 0;
}

static bool dm_agent_eid_is_set(amxd_object_t* obj) {
    char* eid = amxd_object_get_value(cstring_t, obj, "EndpointID", NULL);
    bool retval = false;

    if((eid != NULL) && (*eid != 0)) {
        retval = true;
        SAH_TRACEZ_INFO(ME, "LocalAgent.EndpointID is set to %s from save file", eid);
    }

    free(eid);
    return retval;
}

void dm_agent_set_endpoint_id(amxo_parser_t* parser) {
    int retval = -1;
    const char* auth_scheme = GET_CHAR(&parser->config, "authority-scheme");
    amxd_object_t* obj = amxd_dm_findf(agent_get_dm(), "LocalAgent.");
    amxc_string_t endpoint_id;
    amxd_trans_t transaction;
    get_endpoint_id_t fn[] = {
        {"oui", get_oui_id},
        {"oui_ext", get_oui_ext_id},
        {"ops", get_ops_id},
        {"proto", get_proto_id},
        {NULL, NULL},
    };

    if(auth_scheme == NULL) {
        auth_scheme = "proto";
    }

    amxc_string_init(&endpoint_id, 0);
    amxd_trans_init(&transaction);

    when_true(dm_agent_eid_is_set(obj), exit);

    for(int i = 0; fn[i].authority_scheme != NULL; i++) {
        if(strcmp(fn[i].authority_scheme, auth_scheme) == 0) {
            if(fn[i].fn(&endpoint_id) != 0) {
                SAH_TRACEZ_WARNING(ME, "Error occured with authority scheme %s, using default proto scheme instead", auth_scheme);
                get_proto_id(&endpoint_id);
            }
            break;
        }
    }
    amxc_string_replace(&endpoint_id, " ", "_", UINT32_MAX);
    SAH_TRACEZ_INFO(ME, "Endpoint ID %s", amxc_string_get(&endpoint_id, 0));

    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&transaction, obj);
    amxd_trans_set_value(cstring_t, &transaction,
                         "EndpointID", amxc_string_get(&endpoint_id, 0));
    retval = amxd_trans_apply(&transaction, agent_get_dm());
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "Failed to set LocalAgent.EndpointID = %s",
                           amxc_string_get(&endpoint_id, 0));
    }

exit:
    amxd_trans_clean(&transaction);
    amxc_string_clean(&endpoint_id);
}

void dm_agent_start_uptime(void) {
    amxc_ts_now(&start_time);
}

amxd_status_t _UpTime_uptime_read(UNUSED amxd_object_t* object,
                                  UNUSED amxd_param_t* param,
                                  UNUSED amxd_action_t reason,
                                  UNUSED const amxc_var_t* const args,
                                  amxc_var_t* const retval,
                                  UNUSED void* priv) {
    amxc_ts_t now;

    amxc_ts_now(&now);
    amxc_var_set(int64_t, retval, now.sec - start_time.sec);

    return amxd_status_ok;
}

amxd_status_t _SendTransferComplete(amxd_object_t* object,
                                    UNUSED amxd_function_t* func,
                                    amxc_var_t* args,
                                    UNUSED amxc_var_t* ret) {
    amxc_var_t data;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_set_key(&data, "data", args, AMXC_VAR_FLAG_COPY);

    amxd_object_send_signal(object, "TransferComplete!", &data, false);

    amxc_var_clean(&data);
    return amxd_status_ok;
}
