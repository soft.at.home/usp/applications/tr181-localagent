/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "localagent.h"
#include "dm_subscription.h"

typedef amxd_status_t (* notification_check_fn_t) (amxc_var_t* paths);

typedef struct _subscription_check {
    const char* notification_type;
    notification_check_fn_t fn;
} subscription_check_t;

static amxd_status_t notif_type_value_change(amxc_var_t* paths) {
    amxd_status_t status = amxd_status_ok;
    amxc_var_for_each(it, paths) {
        const char* str_path = amxc_var_constcast(cstring_t, it);
        amxd_path_t path;
        amxd_path_init(&path, str_path);
        if(!(amxd_path_is_search_path(&path) || amxd_path_is_object_path(&path))) {
            status = amxd_status_invalid_value;
            amxd_path_clean(&path);
            break;
        }
        amxd_path_clean(&path);
    }
    return status;
}

static bool check_multiple_instance(amxc_var_t* paths) {
    bool multi = true;
    amxc_var_t* list_entry = NULL;
    const amxc_htable_t* table = NULL;
    amxc_array_t* array = NULL;
    const char* key = NULL;
    amxc_var_t* object_path_params = NULL;

    list_entry = GETI_ARG(paths, 0);
    table = amxc_var_constcast(amxc_htable_t, list_entry);
    array = amxc_htable_get_sorted_keys(table);
    key = (const char*) amxc_array_get_data_at(array, 0);
    object_path_params = amxc_var_get_key(list_entry, key, AMXC_VAR_FLAG_DEFAULT);
    multi = GET_BOOL(object_path_params, "is_multi_instance");
    amxc_array_delete(&array, NULL);
    return multi;
}

static amxd_status_t notif_type_operation_complete(UNUSED amxc_var_t* paths) {
    amxd_status_t status = amxd_status_ok;
    //Todo verification
    return status;
}

static amxd_status_t notif_type_event(UNUSED amxc_var_t* paths) {
    amxd_status_t status = amxd_status_ok;
    //Todo verification
    return status;
}

static amxd_status_t notif_type_object_creation(amxc_var_t* paths) {
    amxd_status_t status = amxd_status_ok;
    amxc_var_t object_path;
    char* check_path = NULL;

    amxc_var_init(&object_path);
    amxc_var_for_each(it, paths) {
        amxd_path_t path;
        amxd_path_init(&path, amxc_var_constcast(cstring_t, it));
        if(amxd_path_is_instance_path(&path)) {
            status = amxd_status_invalid_value;
            amxd_path_clean(&path);
            break;
        }
        if(amxd_path_is_search_path(&path)) {
            check_path = amxd_path_build_supported_path(&path);
        } else {
            check_path = amxc_var_dyncast(cstring_t, it);
        }
        amxb_get_supported(agent_get_busctxt(), check_path, AMXB_FLAG_FIRST_LVL, &object_path, 10);
        if(!check_multiple_instance(&object_path)) {
            status = amxd_status_invalid_value;
            amxd_path_clean(&path);
            break;
        }
        amxd_path_clean(&path);
    }
    amxc_var_clean(&object_path);
    free(check_path);
    return status;
}

static amxd_status_t notif_type_object_deletion(amxc_var_t* paths) {
    amxd_status_t status = amxd_status_ok;
    amxc_var_t object_path;
    char* check_path = NULL;

    amxc_var_init(&object_path);
    amxc_var_for_each(it, paths) {
        amxd_path_t path;
        amxd_path_init(&path, amxc_var_constcast(cstring_t, it));
        if(amxd_path_is_instance_path(&path)) {
            status = amxd_status_invalid_value;
            amxd_path_clean(&path);
            break;
        }
        if(amxd_path_is_search_path(&path)) {
            check_path = amxd_path_build_supported_path(&path);
        } else {
            check_path = amxc_var_dyncast(cstring_t, it);
        }
        amxb_get_supported(agent_get_busctxt(), check_path, AMXB_FLAG_FIRST_LVL, &object_path, 10);
        if(!check_multiple_instance(&object_path)) {
            status = amxd_status_invalid_value;
            amxd_path_clean(&path);
            break;
        }
        amxd_path_clean(&path);
    }
    amxc_var_clean(&object_path);
    free(check_path);
    return status;
}

static int subscription_set_persistent(UNUSED amxd_object_t* object,
                                       amxd_object_t* mobject,
                                       UNUSED void* priv) {
    return amxd_object_set_attr(mobject, amxd_oattr_persistent, true);
}

amxd_status_t _check_subscription(amxd_object_t* object,
                                  UNUSED amxd_param_t* param,
                                  UNUSED amxd_action_t reason,
                                  UNUSED const amxc_var_t* const args,
                                  UNUSED amxc_var_t* const retval,
                                  UNUSED void* priv) {
    amxd_status_t status = amxd_status_function_not_implemented;
    amxc_var_t path_var;
    amxc_var_t params;
    const char* reference_list = NULL;
    const char* notif_type = NULL;
    subscription_check_t fn[] = {
        {"ValueChange", notif_type_value_change},
        {"ObjectCreation", notif_type_object_creation},
        {"ObjectDeletion", notif_type_object_deletion},
        {"OperationComplete", notif_type_operation_complete},
        {"Event", notif_type_event},
        {NULL, NULL},
    };

    amxc_var_init(&path_var);
    amxc_var_init(&params);

    when_true(reason != action_object_validate, exit);

    amxd_object_get_params(object, &params, amxd_dm_access_protected);
    when_false_status(GET_BOOL(&params, "Enable"), exit, status = amxd_status_ok);

    reference_list = GET_CHAR(&params, "ReferenceList");
    amxc_var_set(csv_string_t, &path_var, reference_list);
    amxc_var_cast(&path_var, AMXC_VAR_ID_LIST);

    notif_type = GET_CHAR(&params, "NotifType");
    for(int i = 0; fn[i].notification_type != NULL; i++) {
        if(strcmp(fn[i].notification_type, notif_type) == 0) {
            status = fn[i].fn(&path_var);
            break;
        }
    }
exit:
    amxc_var_clean(&path_var);
    amxc_var_clean(&params);

    return status;
}

amxd_status_t _dm_subscription_assign_default(amxd_object_t* object,
                                              amxd_param_t* param,
                                              amxd_action_t reason,
                                              const amxc_var_t* const args,
                                              amxc_var_t* const retval,
                                              void* priv) {
    amxc_var_t* params = GET_ARG(args, "parameters");
    const char* id = GET_CHAR(params, "ID");
    // If an index is provided in the args or retval, use this one
    uint32_t index = GET_UINT32(args, "index") == 0 ? GET_UINT32(retval, "index") :
        GET_UINT32(args, "index");
    // If there is no index in the args or retval, take the next index
    uint32_t new_index = index == 0 ? (object->last_index + 1) : index;

    if((id == NULL) || (*id == 0)) {
        amxc_string_t id_name;
        amxc_string_init(&id_name, 0);
        amxc_string_setf(&id_name, "cpe-ID-%d", new_index);
        amxc_var_add_key(cstring_t, params, "ID", amxc_string_get(&id_name, 0));
        amxc_string_clean(&id_name);
    }

    return amxd_action_object_add_inst(object, param, reason, args, retval, priv);
}

static void subscription_stop_and_clean_ttl_timer(void* const priv) {
    amxp_timer_t* timer = (amxp_timer_t*) priv;
    when_null(timer, exit);

    amxp_timer_delete(&timer);
    timer = NULL;

exit:
    return;
}

static void subscription_ttl_trigger(UNUSED amxp_timer_t* timer,
                                     void* priv) {
    amxd_object_t* sub = (amxd_object_t*) priv;
    int status = amxd_status_unknown_error;
    amxc_var_t ret;
    const char* sub_path = "LocalAgent.Subscription.";

    when_null(sub, exit);
    SAH_TRACEZ_INFO(ME, "Time to live is elapsed for [%s]", sub->name);
    amxc_var_init(&ret);

    status = amxb_del(amxb_be_who_has(sub_path), sub_path, sub->index, NULL, &ret, 5);
    if(status) {
        SAH_TRACEZ_ERROR(ME, "Error [%s] occured when removing [%s%d]", amxd_status_string(status), sub_path, sub->index);
    }

    amxc_var_clean(&ret);

exit:
    return;
}

static void subscription_handle_time_to_live(amxd_object_t* sub_obj, uint32_t time_to_live) {
    amxp_timer_t* time_to_live_timer = NULL;
    when_null(sub_obj, exit);
    subscription_stop_and_clean_ttl_timer(sub_obj->priv);

    amxp_timer_new(&time_to_live_timer, subscription_ttl_trigger, sub_obj);
    sub_obj->priv = time_to_live_timer;
    amxp_timer_start(time_to_live_timer, (time_to_live * 1000));

exit:
    return;
}

void _Subscription_Added(UNUSED const char* const sig_name,
                         const amxc_var_t* const data,
                         UNUSED void* const priv) {
    amxd_dm_t* dm = agent_get_dm();
    bool persistent = GETP_BOOL(data, "parameters.Persistent");
    uint32_t index = GET_UINT32(data, "index");
    uint32_t time_to_live = GETP_UINT32(data, "parameters.TimeToLive");
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    object = amxd_object_get_instance(object, NULL, index);

    if(persistent) {
        amxd_object_set_attr(object, amxd_oattr_persistent, true);
    }

    if(time_to_live > 0) {
        subscription_handle_time_to_live(object, time_to_live);
    }
}

void _Subscription_Changed(UNUSED const char* const sig_name,
                           const amxc_var_t* const data,
                           UNUSED void* const priv) {
    amxd_dm_t* dm = agent_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    amxc_var_t* persistent = GETP_ARG(data, "parameters.Persistent.to");
    amxc_var_t* ttl = GETP_ARG(data, "parameters.TimeToLive.to");

    if(persistent != NULL) {
        amxd_object_set_attr(object, amxd_oattr_persistent, amxc_var_dyncast(bool, persistent));
    }
    if(ttl != NULL) {
        subscription_handle_time_to_live(object, amxc_var_dyncast(uint32_t, ttl));
    }
}

amxd_status_t _dm_subscription_cleanup(amxd_object_t* object,
                                       UNUSED amxd_param_t* param,
                                       amxd_action_t reason,
                                       UNUSED const amxc_var_t* const args,
                                       UNUSED amxc_var_t* const retval,
                                       UNUSED void* priv) {
    amxd_status_t status = amxd_status_ok;
    when_null(object, exit);
    when_null(object->priv, exit);

    if(reason != action_object_destroy) {
        status = amxd_status_function_not_implemented;
        goto exit;
    }
    subscription_stop_and_clean_ttl_timer(object->priv);

exit:
    return status;
}

void dm_subscription_init(void) {
    amxd_object_t* object = amxd_dm_findf(agent_get_dm(), "LocalAgent.Subscription.");
    amxd_object_for_all(object, "[Persistent == true]", subscription_set_persistent, NULL);
}
