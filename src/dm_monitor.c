/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "localagent.h"
#include "dm_monitor.h"

typedef struct _la_monitor {
    amxc_llist_it_t it;
    char* name;
    char* reference_list;
    uint32_t interval;
    uint32_t changeset_counter;
    amxc_var_t event_data;
    amxp_timer_t* timer;
    amxd_object_t* object;
} la_monitor_t;

amxc_llist_t monitor_list;

static void monitor_subscription_cb(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    UNUSED void* const priv) {
    uint32_t parameter_counter = 1;
    la_monitor_t* monitor = (la_monitor_t*) priv;
    amxc_ts_t current_time;
    amxc_string_t build_string;
    amxc_var_t* updated_params = NULL;
    amxc_var_t* data_var = NULL;

    monitor->changeset_counter++;
    updated_params = GET_ARG(data, "parameters");

    amxc_string_init(&build_string, 0);
    amxc_string_setf(&build_string, "ChangeSet.%d.ObjectPath", monitor->changeset_counter);
    data_var = GET_ARG(&monitor->event_data, "data");
    amxc_var_add_key(cstring_t, data_var, amxc_string_get(&build_string, 0), GET_CHAR(data, "path"));

    amxc_var_for_each(param, updated_params) {
        amxc_string_setf(&build_string, "ChangeSet.%d.Parameter.%d.Name", monitor->changeset_counter, parameter_counter);
        amxc_var_add_key(cstring_t, data_var, amxc_string_get(&build_string, 0), amxc_var_key(param));
        amxc_string_setf(&build_string, "ChangeSet.%d.Parameter.%d.Value", monitor->changeset_counter, parameter_counter);
        amxc_var_add_key(cstring_t, data_var, amxc_string_get(&build_string, 0), GET_CHAR(param, "to"));
        amxc_string_setf(&build_string, "ChangeSet.%d.Parameter.%d.OldValue", monitor->changeset_counter, parameter_counter);
        amxc_var_add_key(cstring_t, data_var, amxc_string_get(&build_string, 0), GET_CHAR(param, "from"));
        amxc_string_setf(&build_string, "ChangeSet.%d.Parameter.%d.ChangeTime", monitor->changeset_counter, parameter_counter);
        amxc_ts_now(&current_time);
        amxc_var_add_key(amxc_ts_t, data_var, amxc_string_get(&build_string, 0), &current_time);
        parameter_counter++;
    }

    amxc_string_clean(&build_string);
}

static void monitor_unsubscribe(la_monitor_t* monitor) {
    amxc_var_t path_var;
    amxc_var_init(&path_var);

    amxc_var_set(csv_string_t, &path_var, monitor->reference_list);
    amxc_var_cast(&path_var, AMXC_VAR_ID_LIST);

    amxc_var_for_each(it, &path_var) {
        amxd_path_t path;
        const char* full_path = amxc_var_constcast(cstring_t, it);
        char* fixed_path = NULL;

        amxd_path_init(&path, full_path);
        fixed_path = amxd_path_get_fixed_part(&path, false);

        amxb_unsubscribe(amxb_be_who_has(fixed_path),
                         full_path,
                         monitor_subscription_cb,
                         monitor);
        amxd_path_clean(&path);
        free(fixed_path);
    }
    amxc_var_clean(&path_var);
}

static void monitor_subscribe(la_monitor_t* monitor,
                              amxc_var_t* params) {
    amxc_var_t path_var;

    amxc_var_init(&path_var);
    amxc_var_set(csv_string_t, &path_var, GET_CHAR(params, "ReferenceList"));
    amxc_var_cast(&path_var, AMXC_VAR_ID_LIST);

    amxc_var_for_each(it, &path_var) {
        amxd_path_t path;
        amxc_string_t expr;
        const char* full_path = amxc_var_constcast(cstring_t, it);
        const char* object_path = NULL;
        const char* param = NULL;

        amxd_path_init(&path, full_path);
        object_path = amxd_path_get(&path, AMXD_OBJECT_TERMINATE);

        amxc_string_init(&expr, 0);
        amxc_string_setf(&expr, "(notification == 'dm:object-changed')");
        param = amxd_path_get_param(&path);
        if(param != NULL) {
            amxc_string_appendf(&expr, " && contains('parameters.%s')", param);
        }

        amxb_subscribe(amxb_be_who_has(object_path),
                       object_path,
                       amxc_string_get(&expr, 0),
                       monitor_subscription_cb,
                       monitor);
        amxd_path_clean(&path);
        amxc_string_clean(&expr);

    }
    amxc_var_clean(&path_var);
}

static void monitor_interval_timeout(UNUSED amxp_timer_t* timer, void* priv) {
    la_monitor_t* monitor = (la_monitor_t*) priv;
    if(GETP_ARG(&monitor->event_data, "data.0") != NULL) {
        amxd_object_send_signal(monitor->object,
                                "OnChange!",
                                &monitor->event_data,
                                true);
        amxc_var_set_type(&monitor->event_data, AMXC_VAR_ID_HTABLE);

        amxc_var_add_key(amxc_htable_t, &monitor->event_data, "data", NULL);
        monitor->changeset_counter = 0;
    }
    amxp_timer_start(monitor->timer, monitor->interval / 1000);
}

static la_monitor_t* create_new_monitor(amxc_var_t* params) {
    la_monitor_t* monitor = calloc(1, sizeof(la_monitor_t));
    when_null(monitor, exit);
    monitor->name = strdup(GET_CHAR(params, "Alias"));
    monitor->reference_list = strdup(GET_CHAR(params, "ReferenceList"));
    monitor->interval = GET_UINT32(params, "Interval");
    monitor->changeset_counter = 0;
    monitor->timer = NULL;
    monitor->object = NULL;

    amxc_var_init(&monitor->event_data);
    amxc_var_set_type(&monitor->event_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(amxc_htable_t, &monitor->event_data, "data", NULL);

    amxp_timer_new(&monitor->timer, monitor_interval_timeout, monitor);
    amxp_timer_start(monitor->timer, monitor->interval / 1000);
exit:
    return monitor;
}

static la_monitor_t* get_monitor(const char* name) {
    la_monitor_t* monitor = NULL;
    if(!name || !*name) {
        SAH_TRACEZ_WARNING(ME, "Invalid name provided");
        return NULL;
    }
    amxc_llist_for_each(lit, &monitor_list) {
        monitor = amxc_llist_it_get_data(lit, la_monitor_t, it);
        if(monitor && monitor->name && *(monitor->name) && (strcmp(name, monitor->name) == 0)) {
            return monitor;
        }
    }
    SAH_TRACEZ_INFO(ME, "Monitor %s not found", name);
    return NULL;
}

void dm_monitor_init(void) {
    amxc_llist_init(&monitor_list);
}

static void dm_monitor_clean_monitorlist(amxc_llist_it_t* it) {
    la_monitor_t* monitor = amxc_container_of(it, la_monitor_t, it);
    SAH_TRACEZ_INFO(ME, "Removing monitor with name %s", monitor->name);
    free(monitor->name);
    free(monitor->reference_list);
    amxc_var_clean(&monitor->event_data);
    amxp_timer_delete(&monitor->timer);
    monitor->timer = NULL;
    monitor->object = NULL;
    free(monitor);
}

void dm_monitor_clean(void) {
    amxc_llist_clean(&monitor_list, dm_monitor_clean_monitorlist);
}

amxd_status_t _check_monitor(amxd_object_t* object,
                             UNUSED amxd_param_t* param,
                             UNUSED amxd_action_t reason,
                             UNUSED const amxc_var_t* const args,
                             UNUSED amxc_var_t* const retval,
                             UNUSED void* priv) {
    amxd_status_t status = amxd_status_function_not_implemented;
    amxc_var_t path_var;
    amxc_var_t params;
    const char* reference_list = NULL;
    const char* controller = NULL;

    amxc_var_init(&path_var);
    amxc_var_init(&params);

    when_true(reason != action_object_validate, exit);
    when_true_status(amxd_object_get_type(object) == amxd_object_template, exit, status = amxd_status_ok);

    amxd_object_get_params(object, &params, amxd_dm_access_protected);
    reference_list = GET_CHAR(&params, "ReferenceList");
    when_true_status((reference_list == NULL) || (*reference_list == 0), exit, status = amxd_status_invalid_arg);
    controller = GET_CHAR(&params, "Controller");
    when_true_status((controller == NULL) || (*controller == 0), exit, status = amxd_status_invalid_arg);

    amxc_var_set(csv_string_t, &path_var, reference_list);
    amxc_var_cast(&path_var, AMXC_VAR_ID_LIST);

    amxc_var_for_each(it, &path_var) {
        const char* str_path = amxc_var_constcast(cstring_t, it);
        amxc_var_t ret;
        amxc_var_init(&ret);
        int retval = amxb_get(amxb_be_who_has(str_path), str_path, 1, &ret, 5);
        amxc_var_clean(&ret);
        when_false_status(retval == 0, exit, status = retval);
    }
    status = amxd_status_ok;

exit:
    amxc_var_clean(&path_var);
    amxc_var_clean(&params);

    return status;
}

void _Monitor_Changed(UNUSED const char* const sig_name,
                      const amxc_var_t* const data,
                      UNUSED void* const priv) {
    amxd_dm_t* dm = agent_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    amxc_var_t params;
    bool enabled = false;
    la_monitor_t* monitor = NULL;

    amxc_var_init(&params);
    amxd_object_get_params(object, &params, amxd_dm_access_protected);
    enabled = GET_BOOL(&params, "Enable");

    monitor = get_monitor(GET_CHAR(&params, "Alias"));
    if(monitor == NULL) {
        SAH_TRACEZ_ERROR(ME, "Changed monitor not registered");
        return;
    }
    monitor_unsubscribe(monitor);

    if(enabled) {
        monitor->interval = GET_UINT32(&params, "Interval");
        monitor_subscribe(monitor, &params);
    }
    amxc_var_clean(&params);
}

void _Monitor_Added(UNUSED const char* const sig_name,
                    const amxc_var_t* const data,
                    UNUSED void* const priv) {
    amxd_dm_t* dm = agent_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, data);
    amxc_var_t* params = GET_ARG(data, "parameters");
    bool enabled = GET_BOOL(params, "Enable");
    la_monitor_t* monitor = create_new_monitor(params);

    if(monitor != NULL) {
        monitor->object = object;
    }
    amxc_llist_append(&monitor_list, &monitor->it);

    if(enabled) {
        monitor_subscribe(monitor, params);
    }
}