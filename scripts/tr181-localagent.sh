#!/bin/sh

. /usr/lib/amx/scripts/amx_init_functions.sh

name="tr181-localagent"
datamodel_root="LocalAgent"

check_if_already_running(){
    if [ -s /var/run/tr181-localagent.pid ] && [ -d "/proc/$(cat /var/run/tr181-localagent.pid)/fdinfo" ]; then
        echo "tr181-localagent is already started"
        exit 0
    fi
}

case $1 in
    boot)
        check_if_already_running
        process_boot ${name} -D
        ;;
    start)
        check_if_already_running
        process_start ${name} -D
        ;;
    stop)
        process_stop ${name}
        ;;
    shutdown)
        process_shutdown ${name}
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    fail)
        /etc/init.d/uspagent stop
        /etc/init.d/tr181-mqtt stop
        /etc/init.d/tr181-mqtt start
        /etc/init.d/tr181-localagent start
        /etc/init.d/uspagent start
        ;;
    debuginfo)
        process_debug_info ${datamodel_root}
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|debuginfo|restart|fail]"
        ;;
esac
